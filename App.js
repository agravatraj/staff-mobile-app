import React from 'react';
import {SafeAreaView, StyleSheet, Text} from 'react-native';
import {round} from 'react-native-reanimated';
import Router from './Source/Router/index';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Router />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
});
