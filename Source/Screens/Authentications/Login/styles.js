import {StyleSheet} from 'react-native';
import {Colors} from '../../../Utils/Colors';
import Fonts from '../../../Utils/Fonts';
import {Dimens} from '../../../Utils/Dimens';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.lightblue,
  },
  rosterViewStyle: {
    alignItems: 'center',
    flex: 1,
  },
  theTextStyle: {
    fontSize: Dimens.dimen_30,
    color: 'white',
    fontFamily: Fonts.openSansRegular,
    marginTop: Dimens.dimen_50,
  },
  rosterTextStyle: {
    fontSize: Dimens.dimen_40,
    color: 'white',
  },
  View2Style: {
    flex: 1,
    alignItems: 'center',
    paddingHorizontal: Dimens.dimen_20,
  },
  fpStyle: {
    color: Colors.white,
    marginTop: Dimens.dimen_10,
    fontFamily: Fonts.openSansBold,
    fontSize: Dimens.dimen_14,
  },
  btnViewStyle: {marginTop: Dimens.dimen_20},
});

export default styles;
