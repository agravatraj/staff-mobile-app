import React from 'react';
import {View, Text} from 'react-native';
import styles from './styles';
import CustomeTextInput from '../../../Components/CustomeTextInput';
import {Colors} from '../../../Utils/Colors';
import CustomeButton from '../../../Components/CustomeButton';
import {Dimens} from '../../../Utils/Dimens';
import Fonts from '../../../Utils/Fonts';

export default class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      password: '',
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.rosterViewStyle}>
          <Text style={styles.theTextStyle}>
            the
            <Text style={styles.rosterTextStyle}>ROSTER</Text>
          </Text>
        </View>
        <View style={styles.View2Style}>
          <CustomeTextInput
            title="Name"
            placeholder="Enter your name"
            height={50}
            value={this.state.name}
            onChangeText={(txt) => {
              this.setState({name: txt});
            }}
          />
          <CustomeTextInput
            title="Password"
            placeholder="Password"
            height={50}
            value={this.state.password}
            onChangeText={(txt) => {
              this.setState({password: txt});
            }}
            secureTextEntry={true}
          />

          <View style={styles.btnViewStyle}>
            <CustomeButton
              title="LOG IN"
              onPress={() => {
                this.props.navigation.navigate('CustomerDbScreen');
              }}
              color={Colors.white}
              fontFamily={Fonts.openSansLight}
              fontSize={22}
              shadow={true}
              backgroundColor={Colors.green}
              width={Dimens.dimen_200}
              height={Dimens.dimen_50}
              alignItems="center"
              justifyContent="center"
              borderRadius={Dimens.dimen_25}
              borderTopLeftRadius={Dimens.dimen_0}
              elevation={Dimens.dimen_5}
            />
          </View>

          <Text
            style={styles.fpStyle}
            onPress={() => {
              this.props.navigation.navigate('ResetPasswordScreen');
            }}>
            Forgot Password?
          </Text>
        </View>
      </View>
    );
  }
}
