import React from 'react';
import {View, Text, Alert} from 'react-native';
import styles from './styles';
import CustomeTextInput from '../../../Components/CustomeTextInput';
import CustomeButton from '../../../Components/CustomeButton';
import {Dimens} from '../../../Utils/Dimens';
import {Colors} from '../../../Utils/Colors'
import Fonts from '../../../Utils/Fonts'

export default class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.rosterViewStyle}>
          <Text style={styles.Text1Style}>Reset Your Password</Text>
          <Text style={styles.Text2Style}>enter your email</Text>
          <Text
            style={styles.backStyle}
            onPress={() => {
              this.props.navigation.pop();
            }}>
            Back
          </Text>
        </View>
        <View style={styles.View2Style}>
          <CustomeTextInput
            title="Email address "
            placeholder="Enter email address"
            height={50}
            value={this.state.email}
            onChangeText={(txt) => {
              this.setState({email: txt});
            }}
          />

          <View style={{marginTop: Dimens.dimen_20}}>
          <CustomeButton
              title="RESET"
              onPress={() => {
                this.props.navigation.navigate('CustomerDbScreen');
              }}
              color={Colors.white}
              fontFamily={Fonts.openSansLight}
              fontSize={22}
              shadow={true}
              backgroundColor={Colors.green}
              width={Dimens.dimen_200}
              height={Dimens.dimen_50}
              alignItems="center"
              justifyContent="center"
              borderRadius={Dimens.dimen_25}
              borderTopLeftRadius={Dimens.dimen_0}
              elevation={Dimens.dimen_5}
            />
          </View>
        </View>
      </View>
    );
  }
}
