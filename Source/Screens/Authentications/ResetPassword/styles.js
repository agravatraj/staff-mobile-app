import {StyleSheet} from 'react-native';
import {Colors} from '../../../Utils/Colors';
import Fonts from '../../../Utils/Fonts';
import {Dimens} from '../../../Utils/Dimens';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.lightblue,
  },
  rosterViewStyle: {
    alignItems: 'flex-start',
    flex: 1,
    paddingHorizontal: Dimens.dimen_20,
  },
  Text1Style: {
    fontSize: Dimens.dimen_30,
    color: 'white',
    fontFamily: Fonts.openSansRegular,
    marginTop: Dimens.dimen_50,
  },
  Text2Style: {
    fontSize: Dimens.dimen_20,
    color: 'white',
    fontFamily: Fonts.openSansRegular,
  },
  View2Style: {
    flex: 1,
    alignItems: 'center',
    paddingHorizontal: Dimens.dimen_20,
  },
  backStyle: {
    color: Colors.white,
    fontSize: Dimens.dimen_22,
    fontFamily: Fonts.helveticaNeueBold,
    marginTop: Dimens.dimen_10,
  },
});

export default styles;
