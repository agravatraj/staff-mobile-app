import React from 'react';
import {View, ImageBackground, Platform} from 'react-native';
import styles from './styles';
import Images from '../../../Utils/Images';

export default class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount() {
    setTimeout(() => {
      this.myfun();
    }, 3000);
  }
  myfun = () => {
    this.props.navigation.navigate('LoginScreen');
    console.log('myfun called');
  };
  render() {
    return Platform.OS === 'ios' ? (
      <View style={styles.container}>
        <ImageBackground
          source={Images.splashScreen}
          style={styles.ImageBackgroundStyle}
        />
      </View>
    ) : (
      <View style={styles.container}>
        <ImageBackground
          source={Images.splashScreen}
          style={styles.ImageBackgroundStyle}
        />
      </View>
    );
  }
}
