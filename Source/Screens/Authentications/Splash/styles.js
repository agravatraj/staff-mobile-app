import {StyleSheet, Dimensions} from 'react-native';
import {Colors} from '../../../Utils/Colors';

const {height, width} = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  ImageBackgroundStyle: {
    width: width,
    height: height,
    backgroundColor: Colors.white,
  },
});

export default styles;
