/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text, ScrollView, TextInput} from 'react-native';
import Header from '../../../Components/Header';
import styles from './styles';
import HeaderBig from '../../../Components/HeaderBig';
import {Dimens} from '../../../Utils/Dimens';
import {Plus, Send, Photo, Camera} from '../../../Utils/Svg';

export default class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
    };
  }
  render() {
    return (
      <View style={styles.container}>
        <Header onPress={() => this.props.navigation.openDrawer()} />
        <View style={{flex: 1}}>
          <ScrollView bounces={false} contentContainerStyle={{flexGrow: 1}}>
            <View style={styles.container}>
              <HeaderBig
                title={
                  <Text>
                    new <Text style={styles.boldtextStyle}>Message</Text>
                  </Text>
                }
                backButton={true}
                backPress={() => {
                  this.props.navigation.goBack();
                }}
              />
              <View style={styles.container2}>
                <View style={styles.TextInputViewStyle}>
                  <TextInput placeholder="Recipient" />
                  <View>
                    <Plus />
                  </View>
                </View>
                <View style={styles.bottomViewStyle}>
                  <View style={{flex: 1}}>
                    <Photo />
                  </View>
                  <View style={{flex: 1}}>
                    <Camera />
                  </View>
                  <View style={styles.TextInput2ViewStyle}>
                    <TextInput />
                  </View>
                  <View style={{flex: 1, marginLeft: Dimens.dimen_8}}>
                    <Send />
                  </View>
                </View>
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
}
