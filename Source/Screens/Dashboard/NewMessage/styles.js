import {StyleSheet} from 'react-native';
import {Colors} from '../../../Utils/Colors';
import Fonts from '../../../Utils/Fonts';
import {Dimens} from '../../../Utils/Dimens';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  container2: {
    flexGrow: 1,
    paddingVertical: Dimens.dimen_30,
    paddingHorizontal: Dimens.dimen_20,
    justifyContent: 'space-between',
  },
  card1: {
    backgroundColor: Colors.white,
    padding: Dimens.dimen_20,
    elevation: Dimens.dimen_5,
    borderRadius: Dimens.dimen_15,
    shadowColor: Colors.black,
    shadowOffset: {width: 0, height: Dimens.dimen_3},
    shadowOpacity: 0.5,
    shadowRadius: Dimens.dimen_5,
    paddingVertical: Dimens.dimen_15,
    marginHorizontal: Dimens.dimen_15,
    marginVertical: Dimens.dimen_6,
    flexDirection: 'row',
  },
  card1titleStyle: {
    fontSize: Dimens.dimen_14,
    fontFamily: Fonts.HelveticaNeue,
    color: Colors.lightblue,
  },
  cardContentText: {
    fontSize: Dimens.dimen_14,
    fontFamily: Fonts.HelveticaNeueLight,
  },
  boldtextStyle: {
    color: Colors.white,
    fontSize: Dimens.dimen_30,
    fontFamily: Fonts.helveticaNeueBold,
  },

  TextInputViewStyle: {
    backgroundColor: Colors.white,
    elevation: Dimens.dimen_5,
    borderRadius: Dimens.dimen_20,
    shadowColor: Colors.black,
    shadowOffset: {width: 0, height: Dimens.dimen_3},
    shadowOpacity: 0.5,
    shadowRadius: Dimens.dimen_5,
    flexDirection: 'row',
    height: Dimens.dimen_40,
    paddingHorizontal: Dimens.dimen_10,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  TextInput2ViewStyle: {
    backgroundColor: Colors.white,
    elevation: Dimens.dimen_5,
    borderRadius: Dimens.dimen_20,
    shadowColor: Colors.black,
    shadowOffset: {width: 0, height: Dimens.dimen_3},
    shadowOpacity: 0.5,
    shadowRadius: Dimens.dimen_5,
    flexDirection: 'row',
    height: Dimens.dimen_40,
    paddingHorizontal: Dimens.dimen_10,
    alignItems: 'center',
    justifyContent: 'space-between',
    flex: Dimens.dimen_8,
  },
  bottomViewStyle: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export default styles;
