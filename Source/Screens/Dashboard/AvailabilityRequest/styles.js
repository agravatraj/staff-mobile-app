import {StyleSheet, Platform} from 'react-native';
import {Colors} from '../../../Utils/Colors';
import Fonts from '../../../Utils/Fonts';
import {Dimens} from '../../../Utils/Dimens';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  container2: {
    flexGrow: 1,
    paddingTop: Dimens.dimen_30,
  },
  messageButtonViewStyle: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingRight: Dimens.dimen_30,
    paddingBottom: Dimens.dimen_30,
    // backgroundColor:'pink'
  },
  messageButtonStyle: {
    height: Dimens.dimen_60,
    width: Dimens.dimen_60,
    alignSelf: 'flex-end',
  },
  boldtextStyle: {
    color: Colors.white,
    fontSize: Dimens.dimen_30,
    fontFamily: Fonts.helveticaNeueBold,
  },
  card1: {
    backgroundColor: Colors.yellow,
    elevation: Dimens.dimen_5,
    borderRadius: Dimens.dimen_15,
    shadowColor: Colors.black,
    shadowOffset: {width: 0, height: Dimens.dimen_3},
    shadowOpacity: 0.5,
    shadowRadius: Dimens.dimen_5,
    marginHorizontal: Dimens.dimen_15,
    marginVertical: Dimens.dimen_6,
  },
  heading: {
    fontFamily: Fonts.helveticaNeueBold,
    fontSize: Dimens.dimen_16,
  },
  dayStyle: {
    fontFamily: Fonts.helveticaNeueBold,
    fontSize: Dimens.dimen_14,
  },
  dateStyle: {
    fontFamily: Fonts.HelveticaNeue,
    fontSize: Dimens.dimen_14,
  },
  siteStyle: {
    fontFamily: Fonts.SourceSansProBold,
    fontSize: Dimens.dimen_10,
    color: Colors.white,
  },
  siteView: {
    backgroundColor: Colors.lightgreen,
    borderRadius: Dimens.dimen_3,
    paddingHorizontal: Dimens.dimen_5,
  },
  renderViewStyle: {
    flex: 1,
    backgroundColor: Colors.white,
    borderRadius: Dimens.dimen_15,
    marginLeft: Dimens.dimen_10,
    padding: Dimens.dimen_10,
  },
});

export default styles;
