/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  FlatList,
  Alert,
} from 'react-native';
import Header from '../../../Components/Header';
import HeaderBig from '../../../Components/HeaderBig';
import styles from './styles';
import {Plus2} from '../../../Utils/Svg';
import {Colors} from '../../../Utils/Colors';
import Fonts from '../../../Utils/Fonts';

const Mydata = [
  {
    id: '1',
    heading: 'Paid Time off',
    site: true,
    siteName: 'Site 1',
    day1: 'Friday',
    day2: 'Sunday',
    date1: '04/06/1998',
    date2: '22/04/1997',
    backColor: Colors.lightgreen,
  },
  {
    id: '2',
    heading: 'Unavailable',
    site: false,
    siteName: 'Site 2',
    day1: 'Friday',
    day2: 'Sunday',
    date1: '04/06/1998',
    date2: '22/04/1997',
    backColor: Colors.yellow,
  },
  {
    id: '3',
    heading: 'Unavailable',
    site: false,
    siteName: 'Site 3',
    day1: 'Friday',
    day2: 'Sunday',
    date1: '04/06/1998',
    date2: '22/04/1997',
    backColor: Colors.tomato,
  },
];

export default class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={styles.container}>
        <View
          style={{
            //   flex: 1,
            position: 'absolute',
            left: 0,
            right: 0,
            top: 50,
            bottom: 0,
          }}>
          <ScrollView bounces={false} contentContainerStyle={{flexGrow: 1}}>
            <View style={styles.container}>
              <HeaderBig
                title={<Text>Here’s your</Text>}
                title2={'Availability requests'}
                backButton={false}
                backPress={() => {
                  this.props.navigation.goBack();
                }}
              />
              <View style={styles.container2}>
                <FlatList
                  bounces={false}
                  data={Mydata}
                  renderItem={this.renderItem}
                  keyExtractor={(item) => item.id}
                  showsVerticalScrollIndicator={false}
                  style={{flex: 1}}
                  contentContainerStyle={{flexGrow: 1}}
                />
              </View>
            </View>
          </ScrollView>
        </View>
        <Header onPress={() => this.props.navigation.openDrawer()} />
        <View style={styles.messageButtonViewStyle} pointerEvents="box-none">
          <TouchableOpacity
            style={styles.messageButtonStyle}
            onPress={() => {
              this.props.navigation.navigate(
                'AvailabiltyRequestNewRequestScreen',
              );
            }}>
            <Plus2 />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
  renderItem = ({item}) => (
    <TouchableOpacity style={[styles.card1, {backgroundColor: item.backColor}]}>
      <View style={styles.renderViewStyle}>
        <Text style={styles.heading}>{item.heading}</Text>
        {item.site === true ? (
          <View style={{flexDirection: 'row'}}>
            <View style={styles.siteView}>
              <Text style={styles.siteStyle}>{item.siteName}</Text>
            </View>
          </View>
        ) : null}
        <Text style={styles.dayStyle}>
          {item.day1} <Text style={styles.dateStyle}> {item.date1}</Text>{' '}
        </Text>
        <Text style={styles.dayStyle}>
          {item.day2} <Text style={styles.dateStyle}> {item.date2}</Text>{' '}
        </Text>
      </View>
    </TouchableOpacity>
  );
}
