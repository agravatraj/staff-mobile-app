/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Text,
  TextInput,
  KeyboardAvoidingView,
  ScrollView,
} from 'react-native';
import Header from '../../../Components/Header';
import styles from './styles';
import HeaderBig from '../../../Components/HeaderBig';
import CustomeButton from '../../../Components/CustomeButton';
import {Colors} from '../../../Utils/Colors';
import Fonts from '../../../Utils/Fonts';
import {Dimens} from '../../../Utils/Dimens';

export default class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <KeyboardAvoidingView style={styles.container}>
        <ScrollView contentContainerStyle={{flexGrow: 1}}>
          <View style={styles.container}>
            <Header onPress={() => this.props.navigation.openDrawer()} />
            <HeaderBig
              title={
                <Text>
                  How can we <Text style={styles.boldtextStyle}>Help</Text>
                </Text>
              }
              backButton={true}
              backPress={() => {
                this.props.navigation.goBack();
              }}
            />
            <View style={styles.container2}>
              <View style={styles.card1}>
                <Text style={styles.card1titleStyle}>
                  For any help or enquiries, please let us know and we will get
                  back to you in 2-3 business days
                </Text>
              </View>

              <View style={styles.card2}>
                <TextInput placeholder="Type here" multiline={true} />
              </View>
              <View style={styles.privacyView}>
                <Text
                  style={styles.card1titleStyle}
                  onPress={() => {
                    this.props.navigation.navigate('PrivacyScreen');
                  }}>
                  Privacy
                </Text>
                <Text
                  style={styles.card1titleStyle}
                  onPress={() => {
                    this.props.navigation.navigate('UserAgreementScreen');
                  }}>
                  Terms & condition
                </Text>
              </View>
              <View style={styles.btnViewStyle}>
                <CustomeButton
                  title="Submit"
                  color={Colors.white}
                  fontFamily={Fonts.HelveticaNeueMedium}
                  fontSize={Dimens.dimen_16}
                  backgroundColor={Colors.orange}
                  borderRadius={Dimens.dimen_18}
                  height={Dimens.dimen_45}
                  width={Dimens.dimen_125}
                />
              </View>
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}
