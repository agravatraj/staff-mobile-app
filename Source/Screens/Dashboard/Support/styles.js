import {StyleSheet, Platform} from 'react-native';
import {Colors} from '../../../Utils/Colors';
import Fonts from '../../../Utils/Fonts';
import {Dimens} from '../../../Utils/Dimens';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  container2: {
    flexGrow: 1,
    paddingHorizontal: Dimens.dimen_15,
    marginVertical: Dimens.dimen_25,
  },
  card1: {
    backgroundColor: Colors.white,
    justifyContent: 'center',
    padding: Dimens.dimen_20,
    elevation: Dimens.dimen_5,
    borderRadius: Dimens.dimen_15,
    shadowColor: Colors.black,
    shadowOffset: {width: 0, height: Dimens.dimen_3},
    shadowOpacity: 0.5,
    shadowRadius: Dimens.dimen_5,
    paddingVertical: Dimens.dimen_20,
    marginBottom: Dimens.dimen_20,
  },
  card2: {
    backgroundColor: Colors.white,
    elevation: Dimens.dimen_5,
    borderRadius: Dimens.dimen_15,
    shadowColor: Colors.black,
    shadowOffset: {width: 0, height: Dimens.dimen_3},
    shadowOpacity: 0.5,
    shadowRadius: Dimens.dimen_5,
    marginBottom: Dimens.dimen_20,
    paddingHorizontal: Dimens.dimen_20,
    paddingVertical: Platform.OS === 'ios' ? Dimens.dimen_20 : 0,
    flex: 1,
  },
  card1_1: {
    flexDirection: 'row',
    marginTop: Dimens.dimen_8,
    alignItems: 'stretch',
    justifyContent: 'space-around',
  },
  card2_2: {
    marginTop: Dimens.dimen_8,
  },
  card1titleStyle: {
    fontSize: Dimens.dimen_14,
    fontFamily: Fonts.HelveticaNeue,
    color: Colors.lightblue,
  },
  innerview1Style: {
    backgroundColor: Colors.lightgrey,
    padding: Dimens.dimen_20,
    borderRadius: Dimens.dimen_10,
    alignItems: 'center',
    marginHorizontal: Dimens.dimen_15,
    width: Dimens.dimen_150,
  },
  number: {
    fontSize: Dimens.dimen_18,
    fontFamily: Fonts.helveticaNeueBold,
  },
  shift: {
    fontSize: Dimens.dimen_16,
    fontFamily: Fonts.HelveticaNeueLight,
  },
  customerText: {
    fontSize: Dimens.dimen_16,
    fontFamily: Fonts.helveticaNeueBold,
    marginBottom: Platform.OS === 'ios' ? Dimens.dimen_5 : 0,
  },
  siteText: {
    fontSize: Dimens.dimen_10,
    fontFamily: Fonts.helveticaNeueBold,
    marginVertical: 1,
  },
  smallView1: {
    backgroundColor: Colors.lightgreen,
    paddingHorizontal: Dimens.dimen_8,
    borderRadius: Dimens.dimen_5,
    marginRight: Dimens.dimen_5,
  },
  smallView2: {
    backgroundColor: Colors.sky,
    paddingHorizontal: Dimens.dimen_8,
    borderRadius: Dimens.dimen_5,
  },
  View2style: {
    flexDirection: 'row',
    marginBottom: Platform.OS === 'ios' ? Dimens.dimen_5 : 0,
  },
  dateTimeStyle: {
    fontSize: Dimens.dimen_16,
    fontFamily: Fonts.HelveticaNeue,
    marginHorizontal: Dimens.dimen_5,
    marginBottom: Dimens.dimen_15,
  },
  buttonView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  boldtextStyle: {
    color: Colors.white,
    fontSize: Dimens.dimen_30,
    fontFamily: Fonts.helveticaNeueBold,
  },
  privacyView: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  btnViewStyle: {
    alignItems: 'flex-end',
    marginTop: Dimens.dimen_40,
  },
});

export default styles;
