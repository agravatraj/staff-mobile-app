/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text, ScrollView, TextInput, Alert} from 'react-native';
import Header from '../../../Components/Header';
import styles from './styles';
import HeaderBig from '../../../Components/HeaderBig';
import {Dimens} from '../../../Utils/Dimens';
import {Plus, Send, Photo, Camera, DropdownArrow} from '../../../Utils/Svg';
import CustomeButton from '../../../Components/CustomeButton';
import {Colors} from '../../../Utils/Colors';
import Fonts from '../../../Utils/Fonts';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Modal from 'react-native-modal';
import PushPopButton from '../../../Components/PushPopButton';
import CustomeRadioButton from '../../../Components/CustomeRadioButton';

export default class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      modalVisible: false,
      incidentSummaryReport: '',
      locationOfIncident: '',
      incidentDetail: '',
      managerComment: '',
      managerDetail: '',
    };
  }
  render() {
    return (
      <View style={styles.container}>
        <Header onPress={() => this.props.navigation.openDrawer()} />
        <View style={{flex: 1}}>
          <ScrollView
            bounces={false}
            contentContainerStyle={{flexGrow: 1}}
            keyboardShouldPersistTaps={'always'}>
            <View style={styles.container}>
              <HeaderBig
                title={
                  <Text>
                    New{' '}
                    <Text style={styles.boldtextStyle}>Incident reports</Text>
                  </Text>
                }
                backButton={true}
                backPress={() => {
                  this.props.navigation.goBack();
                }}
              />
              <View style={styles.container2}>
                <View style={styles.card1}>
                  <Text style={styles.labelStyle}>Action taken/ details</Text>
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({modalVisible: true});
                    }}>
                    <DropdownArrow />
                  </TouchableOpacity>
                  <Modal
                    backdropOpacity={0}
                    style={{
                      justifyContent: 'center',
                      paddingBottom: 20,
                      paddingHorizontal: 30,
                    }}
                    isVisible={this.state.modalVisible}
                    onBackdropPress={() => {
                      this.setState({modalVisible: false});
                    }}>
                    <View style={styles.modalView}>
                      <View style={styles.modalView1_1}>
                        <PushPopButton title="Patron refused entry" />
                        <PushPopButton title="Patron refused service" />
                        <PushPopButton title="Patron asked to leave" />
                        <PushPopButton title="First aid treatment supplied" />
                        <PushPopButton title="Ambulance attended" />
                        <PushPopButton title="Security attended" />
                        <PushPopButton title="Police called by venue staff" />
                        <PushPopButton title="Police involved" />
                      </View>
                      <View style={styles.cancelViewStyle}>
                        <Text
                          style={styles.cancel}
                          onPress={() => {
                            this.setState({modalVisible: false});
                          }}>
                          Cancel
                        </Text>
                        <Text style={styles.add}>Add</Text>
                      </View>
                    </View>
                  </Modal>
                </View>
                <View style={[styles.card1, {flexDirection: 'column'}]}>
                  <Text style={styles.labelStyle}>Incident summary report</Text>
                  <View style={{paddingLeft: 30, paddingTop: 10}}>
                    <TextInput
                      placeholder=""
                      multiline={true}
                      style={styles.TextInputTextStyle}
                      onChangeText={(txt) => {
                        this.setState({incidentSummaryReport: txt});
                      }}
                    />
                  </View>
                </View>
                <View style={styles.card1}>
                  <View style={styles.leftView}>
                    <Text style={styles.labelStyle}>
                      No of Persons Involved
                    </Text>
                  </View>
                  <View style={styles.rightView}>
                    <CustomeRadioButton />
                  </View>
                </View>
                <View style={styles.card1}>
                  <View style={styles.leftView}>
                    <Text style={styles.labelStyle}>Captured on CCTV?</Text>
                  </View>
                  <View style={styles.rightView}>
                    <CustomeRadioButton />
                  </View>
                </View>
                <View style={styles.card1}>
                  <View style={styles.leftView}>
                    <Text style={styles.labelStyle}>CCTV burned to disc?</Text>
                  </View>
                  <View style={styles.rightView}>
                    <CustomeRadioButton />
                  </View>
                </View>
                <View style={styles.card1}>
                  <View style={styles.leftView}>
                    <Text style={styles.labelStyle}>
                      Linked to other incidents
                    </Text>
                  </View>
                  <View style={styles.rightView}>
                    <CustomeRadioButton />
                  </View>
                </View>
                <View style={styles.card1}>
                  <View style={{flex: 1}}>
                    <Text style={styles.labelStyle}>Location of incident</Text>
                  </View>
                  <View style={{flex: 1}}>
                    <TextInput
                      onChangeText={(txt) => {
                        this.setState({locationOfIncident: txt});
                      }}
                      style={styles.TextInputTextStyle}
                    />
                  </View>
                </View>
                <View style={styles.card1}>
                  <View style={{flex: 1}}>
                    <Text style={styles.labelStyle}>Incident details</Text>
                  </View>
                  <View style={{flex: 1}}>
                    <TextInput
                      onChangeText={(txt) => {
                        this.setState({incidentDetail: txt});
                      }}
                      style={styles.TextInputTextStyle}
                    />
                  </View>
                </View>
                <View style={styles.card1}>
                  <View style={{flex: 1}}>
                    <Text style={styles.labelStyle}>Manager comments</Text>
                  </View>
                  <View style={{flex: 1}}>
                    <TextInput
                      onChangeText={(txt) => {
                        this.setState({managerComment: txt});
                      }}
                      style={styles.TextInputTextStyle}
                    />
                  </View>
                </View>
                <View style={styles.card1}>
                  <View style={styles.leftView}>
                    <Text style={styles.labelStyle}>Manager approved?</Text>
                  </View>
                  <View style={styles.rightView}>
                    <CustomeRadioButton />
                  </View>
                </View>
                <View style={styles.card1}>
                  <View style={{flex: 1}}>
                    <Text style={styles.labelStyle}>Manager details</Text>
                  </View>
                  <View style={{flex: 1}}>
                    <TextInput
                      onChangeText={(txt) => {
                        this.setState({managerDetail: txt});
                      }}
                      style={styles.TextInputTextStyle}
                    />
                  </View>
                </View>
                <View
                  style={{
                    alignSelf: 'flex-end',
                    marginVertical: 20,
                    marginRight: 10,
                  }}>
                  <CustomeButton
                    onPress={() => {
                      // this.props.navigation.navigate(
                      //   'NewIncidentReportPage3Screen',
                      // );
                      // Alert.alert('details submitted');
                      this.CheckValidation() === true
                        ? Alert.alert('details submitted')
                        : null;
                    }}
                    title="Submit incident"
                    backgroundColor={Colors.orange}
                    width={160}
                    height={40}
                    borderRadius={15}
                    color={Colors.white}
                    fontSize={16}
                    fontFamily={Fonts.HelveticaNeueMedium}
                  />
                </View>
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
  CheckValidation = () => {
    if (this.state.incidentSummaryReport === '') {
      Alert.alert('enter incident Summary Report');
      return false;
    }
    if (this.state.locationOfIncident === '') {
      Alert.alert('enter location Of Incident');
      return false;
    }
    if (this.state.incidentDetail === '') {
      Alert.alert('enter incident Detail');
      return false;
    }
    if (this.state.managerComment === '') {
      Alert.alert('enter manager Comment ');
      return false;
    }
    if (this.state.managerDetail === '') {
      Alert.alert('enter manager Detail ');
      return false;
    }
    return true;
  };
}
