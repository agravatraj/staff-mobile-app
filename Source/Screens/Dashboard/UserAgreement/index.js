/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text, ScrollView, FlatList} from 'react-native';
import Header from '../../../Components/Header';
import styles from './styles';
import HeaderBig from '../../../Components/HeaderBig';

const Mydata = [
  {
    id: 1,
    title: 'About the website',
    content:
      'SECCY Pty Ltd (SECCY respects your right to privacy and is committed to safeguarding the privacy of our customers (Customer) and software application (Application) users. We adhere to the Australian Privacy Principles contained in the Privacy Act 1988 (Cth). This policy sets out how we collect and treat your personal information‘Personal Information’ is information we hold which is being about you ',
  },
  {
    id: 2,
    title: '2- Collection of personal Information',
    content:
      'SECCY Pty Ltd (SECCY respects your right to privacy and is committed to safeguarding the privacy of our customers (Customer) and software application (Application) users. We adhere to the Australian Privacy Principles contained in the Privacy Act 1988 (Cth). This policy sets out how we collect and treat your personal information‘Personal Information’ is information we hold which is being about you ',
  },
  {
    id: 3,
    title: '3- How we collect your personal information',
    content:
      'SECCY Pty Ltd (SECCY respects your right to privacy and is committed to safeguarding the privacy of our customers (Customer) and software application (Application) users. We adhere to the Australian Privacy Principles contained in the Privacy Act 1988 (Cth). This policy sets out how we collect and treat your personal information‘Personal Information’ is information we hold which is being about you ',
  },
];

export default class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={styles.container}>
        <Header onPress={() => this.props.navigation.openDrawer()} />
        <ScrollView bounces={false} contentContainerStyle={{flexGrow: 1}}>
          <View style={styles.container}>
            <HeaderBig
              title={
                <Text>
                  User <Text style={styles.boldtextStyle}>Agreement</Text>
                </Text>
              }
              backButton={true}
              backPress={() => {
                this.props.navigation.goBack();
              }}
            />
            <View style={styles.container2}>
              <FlatList
                bounces={false}
                data={Mydata}
                renderItem={this.renderItem}
                keyExtractor={(item) => item.id + ''}
                showsVerticalScrollIndicator={false}
                style={{flex: 1}}
              />
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
  renderItem = ({item}) => (
    <View style={styles.card1}>
      <Text style={styles.cardTitleText}>{item.title}</Text>
      <Text style={styles.cardContentText}>{item.content}</Text>
    </View>
  );
}
