/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  SectionList,
  ScrollView,
} from 'react-native';
import styles from './styles';
import Header from '../../../Components/Header';
import HeaderBig from '../../../Components/HeaderBig';
import {ListDots} from '../../../Utils/Svg';
import CalendarStrip from 'react-native-calendar-strip';
import {
  MenuProvider,
  MenuOption,
  MenuOptions,
  Menu,
  MenuTrigger,
  renderers,
} from 'react-native-popup-menu';
import {Colors} from '../../../Utils/Colors';
const {ContextMenu, SlideInMenu, Popover} = renderers;
const Mydata = [
  {
    title: 'Monday 10/08/2020',
    data: [
      {
        customerName: 'Customer 1',
        siteName: 'Site 1',
        areaName: 'Area 1',
        day: 'Monday',
        date: '10/08/2020',
        time: '08:00pm - 11:30pm',
        shiftConfirm: false,
      },
      {
        customerName: 'Customer 1',
        siteName: 'Site 1',
        areaName: 'Area 1',
        day: 'Monday',
        date: '10/08/2020',
        time: '09:00pm - 11:30pm',
        shiftConfirm: false,
      },
    ],
  },
  {
    title: 'Thursday 13/08/2020',
    data: [
      {
        customerName: 'Customer 1',
        siteName: 'Site 1',
        areaName: 'Area 1',
        day: 'Thursday',
        date: '10/08/2020',
        time: '09:00pm - 11:30pm',
        shiftConfirm: false,
      },
    ],
  },
];

const optionsStyles = {
  optionsContainer: {
    backgroundColor: Colors.white,
    padding: 15,
    borderRadius: 15,
  },
};

class CalenderView extends React.Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <CalendarStrip
          //scrollable
          style={{height: 100, paddingTop: 20, paddingBottom: 10}}
          calendarColor={Colors.lightgreen}
          // calendarHeaderStyle={{color: Colors.black}}
          // dateNumberStyle={{color: 'white'}}
          // dateNameStyle={{color: 'white'}}
          iconContainer={{flex: 0.1}}
        />
      </View>
    );
  }
}

export default class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      opened: false,
      calenderView: false,
    };
  }

  render() {
    return (
      <MenuProvider>
        <View style={styles.container}>
          <Header
            onPress={() => {
              this.props.navigation.openDrawer();
            }}
          />
          <ScrollView bounces={false} contentContainerStyle={{flexGrow: 1}}>
            <HeaderBig
              title={<Text>Here’s your</Text>}
              title2="Past shifts"
              listButton={true}
              listButtonPress={() => {
                this.setState({calenderView: !this.state.calenderView});
              }}
            />
            {this.state.calenderView ? (
              <CalenderView />
            ) : (
              <View style={styles.container2}>
                <View style={styles.SectionListView}>
                  <SectionList
                    sections={Mydata}
                    renderItem={this.renderItem}
                    keyExtractor={(item, index) => item + index}
                    renderSectionHeader={({section: {title}}) => (
                      <Text style={styles.header}>{title}</Text>
                    )}
                    bounces={false}
                  />
                </View>
              </View>
            )}
          </ScrollView>
        </View>
      </MenuProvider>
    );
  }
  renderItem = ({section, index}) => {
    return (
      <View style={styles.cardStyle}>
        <View style={styles.view1}>
          <Text>{section.data[index].customerName}</Text>
          <TouchableOpacity
            style={{paddingHorizontal: 10}}
            onPress={() => {
              this.setState({opened: true});
            }}>
            {this.YourComponent()}
            <ListDots />
          </TouchableOpacity>
        </View>
        <View style={styles.view2}>
          <View style={styles.site1View}>
            <Text style={styles.siteStyle}>{section.data[index].siteName}</Text>
          </View>
          <View style={styles.area1View}>
            <Text style={styles.areaStyle}>{section.data[index].areaName}</Text>
          </View>
          {section.data[index].shiftConfirm ? (
            <View style={styles.shiftConfirm}>
              <Text style={styles.shiftTextStyle}>{'Unconfirmed'}</Text>
            </View>
          ) : null}
        </View>
        <View style={styles.view3}>
          <Text style={styles.day}>{section.data[index].day} </Text>
          <Text style={styles.datetime}>{section.data[index].date}</Text>
          <Text style={styles.datetime}>{section.data[index].time}</Text>
        </View>
      </View>
    );
  };
  YourComponent = () => (
    <Menu
      opened={this.state.opened}
      renderer={Popover}
      onBackdropPress={() => this.setState({opened: false})}
      style={{backgroundColor: 'red'}}>
      <MenuTrigger />
      <MenuOptions customStyles={optionsStyles}>
        <MenuOption
          onSelect={() => this.props.navigation.navigate('ShiftDetailScreen')}
          text="View Details"
        />
        <MenuOption onSelect={() => alert('Delete')}>
          <Text>Confirm shift</Text>
        </MenuOption>
        <MenuOption onSelect={() => alert('Not called')} text="Decline shift" />
      </MenuOptions>
    </Menu>
  );
}
