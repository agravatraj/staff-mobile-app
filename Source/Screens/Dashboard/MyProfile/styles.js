import {StyleSheet, Platform, Dimensions} from 'react-native';
import {Colors} from '../../../Utils/Colors';
import Fonts from '../../../Utils/Fonts';
import {Dimens} from '../../../Utils/Dimens';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  container2: {
    flexGrow: 1,
    paddingVertical: Dimens.dimen_30,
    paddingHorizontal: Dimens.dimen_10,
    justifyContent: 'space-between',
  },
  card1: {
    backgroundColor: Colors.white,
    elevation: Dimens.dimen_5,
    paddingHorizontal: Dimens.dimen_10,
    borderRadius: Dimens.dimen_15,
    shadowColor: Colors.black,
    shadowOffset: {width: 0, height: Dimens.dimen_3},
    shadowOpacity: 0.5,
    shadowRadius: Dimens.dimen_5,
    paddingVertical: Dimens.dimen_15,
  },
  card1titleStyle: {
    fontSize: Dimens.dimen_14,
    fontFamily: Fonts.HelveticaNeue,
    color: Colors.lightblue,
  },
  cardContentText: {
    fontSize: Dimens.dimen_14,
    fontFamily: Fonts.HelveticaNeueLight,
  },
  boldtextStyle: {
    color: Colors.white,
    fontSize: Dimens.dimen_30,
    fontFamily: Fonts.helveticaNeueBold,
  },
  editButtonViewStyle: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingRight: Dimens.dimen_30,
    paddingBottom: Dimens.dimen_30,
    // backgroundColor:'pink'
  },
  editButtonStyle: {
    height: Dimens.dimen_60,
    width: Dimens.dimen_60,
    backgroundColor: Colors.orange,
    borderRadius: Dimens.dimen_30,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'flex-end',
  },
  attachButtonStyle: {
    flexDirection: 'row',
    backgroundColor: Colors.semiWhite2,
    height: Dimens.dimen_45,
    width: Dimens.dimen_250,
    borderRadius: Dimens.dimen_45 / 2,
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: Dimens.dimen_20,
    marginLeft: -1,
  },
  attachButtonViewStyle: {
    backgroundColor: Colors.lightblue,
    height: Dimens.dimen_44,
    width: Dimens.dimen_248,
    borderRadius: Dimens.dimen_45 / 2,
    marginVertical: Dimens.dimen_2,
  },
  SafeAreaViewStyle: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: Colors.semiWhite,
  },
  xStyle: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    paddingRight: Dimens.dimen_20,
  },
  imageStyle: {
    width: Dimensions.get('window').width - 40,
    height: 300,
  },
  xTextStyle: {
    fontSize: Dimens.dimen_45,
    fontFamily: Fonts.HelveticaNeue,
  },
});

export default styles;
