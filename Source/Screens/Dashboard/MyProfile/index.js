/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Text,
  ScrollView,
  FlatList,
  RefreshControl,
  TextInput,
  KeyboardAvoidingView,
  TouchableOpacity,
  BackHandler,
  Alert,
  Platform,
  Image,
  Modal,
  SafeAreaView,
  Dimensions,
} from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import Header from '../../../Components/Header';
import styles from './styles';
import HeaderBig from '../../../Components/HeaderBig';
import {Colors} from '../../../Utils/Colors';
import {Dimens} from '../../../Utils/Dimens';
import {Edit, Save, ImageIcon, Attach} from '../../../Utils/Svg';
import CustomeTextInput2 from '../../../Components/CustomeTextInput2';
import CustomeButton from '../../../Components/CustomeButton';
import Fonts from '../../../Utils/Fonts';
import Images from '../../../Utils/Images';

class AttachButton extends React.Component {
  render() {
    return (
      <View
        style={[
          styles.attachButtonViewStyle,
          {
            backgroundColor: this.props.editClick
              ? Colors.greenForText
              : Colors.lightblue,
          },
        ]}>
        <TouchableOpacity
          style={styles.attachButtonStyle}
          onPress={this.props.onPress}
          disabled={this.props.disable}>
          <Text
            style={{
              color: this.props.editClick
                ? Colors.greenForText
                : Colors.lightblue,
              fontSize: Dimens.dimen_12,
              fontFamily: Fonts.HelveticaNeue,
            }}>
            {this.props.title}
          </Text>
          {typeof this.props.docAray === 'undefined' ? (
            <Attach width={10} height={20} />
          ) : (
            <ImageIcon />
          )}
        </TouchableOpacity>
      </View>
    );
  }
}

export default class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      Email: '',
      Phone: '',
      Dob: '',
      Gender: '',
      LicenceNumber: '',
      Emergency: '',
      Address: '',
      editable: false,
      Edit_or_Save: true,
      editClick: false,
      modalVisible: false,
      document1Path: Images.license,
      documents: [],
      whichImage: null,
    };
  }
  backAction = () => {
    this.setState({editable: false, Edit_or_Save: true, editClick: false});
  };
  componentDidMount() {
    this.backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      this.backAction,
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <Header onPress={() => this.props.navigation.openDrawer()} />
        <View
          style={{
            flex: 1,
            position: 'absolute',
            left: 0,
            right: 0,
            top: 50,
            bottom: 0,
          }}>
          <KeyboardAvoidingView
            style={{flex: 1}}
            behavior={Platform.OS === 'ios' ? 'position' : 'height'}>
            <ScrollView bounces={false} contentContainerStyle={{flexGrow: 1}}>
              <View style={styles.container}>
                <HeaderBig
                  UserDetail={true}
                  title={''}
                  backButton={true}
                  companyButton={this.state.editClick ? false : true}
                  backPress={() => {
                    this.setState({
                      Edit_or_Save: true,
                      editable: false,
                      editClick: false,
                    });
                    this.props.navigation.goBack();
                  }}
                  editIcon={this.state.editClick}
                />
                <View style={styles.container2}>
                  <View style={styles.card1}>
                    <CustomeTextInput2
                      title="Email address"
                      placeholder="Enter Email address"
                      onChangeText={(txt) => {
                        this.setState({Email: txt});
                      }}
                      value={this.state.Email}
                      editable={this.state.editable}
                      editClick={this.state.editClick}
                      placeholderTextColor={
                        this.state.editClick
                          ? Colors.greenForText
                          : Colors.lightblue
                      }
                    />
                    <CustomeTextInput2
                      title="Phone number"
                      placeholder="Enter phone number"
                      onChangeText={(txt) => {
                        this.setState({Phone: txt});
                      }}
                      value={this.state.Phone}
                      editable={this.state.editable}
                      editClick={this.state.editClick}
                      placeholderTextColor={
                        this.state.editClick
                          ? Colors.greenForText
                          : Colors.lightblue
                      }
                    />
                    <CustomeTextInput2
                      title="DOB"
                      placeholder="Enter Date of Birth"
                      onChangeText={(txt) => {
                        this.setState({Dob: txt});
                      }}
                      value={this.state.Dob}
                      editable={this.state.editable}
                      editClick={this.state.editClick}
                      placeholderTextColor={
                        this.state.editClick
                          ? Colors.greenForText
                          : Colors.lightblue
                      }
                    />
                    <CustomeTextInput2
                      title="Gender"
                      placeholder="Enter gender"
                      onChangeText={(txt) => {
                        this.setState({Gender: txt});
                      }}
                      value={this.state.Gender}
                      editable={this.state.editable}
                      editClick={this.state.editClick}
                      placeholderTextColor={
                        this.state.editClick
                          ? Colors.greenForText
                          : Colors.lightblue
                      }
                    />
                    <CustomeTextInput2
                      title="Licence Number"
                      placeholder="Enter Licence Number"
                      onChangeText={(txt) => {
                        this.setState({LicenceNumber: txt});
                      }}
                      value={this.state.LicenceNumber}
                      editable={this.state.editable}
                      editClick={this.state.editClick}
                      placeholderTextColor={
                        this.state.editClick
                          ? Colors.greenForText
                          : Colors.lightblue
                      }
                    />
                    <CustomeTextInput2
                      title="Emergency"
                      placeholder="Emergency Contact Name"
                      onChangeText={(txt) => {
                        this.setState({Emergency: txt});
                      }}
                      value={this.state.Emergency}
                      editable={this.state.editable}
                      editClick={this.state.editClick}
                      placeholderTextColor={
                        this.state.editClick
                          ? Colors.greenForText
                          : Colors.lightblue
                      }
                    />
                    <CustomeTextInput2
                      title="Address"
                      placeholder="Enter address"
                      onChangeText={(txt) => {
                        this.setState({Address: txt});
                      }}
                      value={this.state.Address}
                      height={150}
                      multiline={true}
                      address={true}
                      returnKeyType={'default'}
                      blurOnSubmit={false}
                      editable={this.state.editable}
                      editClick={this.state.editClick}
                      placeholderTextColor={
                        this.state.editClick
                          ? Colors.greenForText
                          : Colors.lightblue
                      }
                    />
                  </View>
                  <CustomeButton
                    title="View Document"
                    backgroundColor={Colors.white}
                    borderRadius={20}
                    height={40}
                    shadow={true}
                    elevation={3}
                    color={Colors.lightblue}
                    fontSize={14}
                    fontFamily={Fonts.helveticaNeueBold}
                    onPress={() => {
                      this.setState({modalVisible: true});
                    }}
                  />
                  <View>
                    <AttachButton
                      title="Security license"
                      editClick={this.state.editClick}
                      onPress={() => {
                        this.ChooseDocument(0);
                      }}
                      disable={!this.state.editClick}
                      docAray={this.state.documents[0]}
                    />
                    <AttachButton
                      title="RSA/RCG certificate"
                      editClick={this.state.editClick}
                      onPress={() => {
                        this.ChooseDocument(1);
                      }}
                      disable={!this.state.editClick}
                      docAray={this.state.documents[1]}
                    />
                    <AttachButton
                      title="First aid certificate"
                      editClick={this.state.editClick}
                      disable={!this.state.editClick}
                      docAray={this.state.documents[2]}
                      onPress={() => {
                        this.ChooseDocument(2);
                        // this.setState({modalVisible: true});
                        // console.log('my array', this.state.documents);
                      }}
                    />
                    <Modal transparent={true} visible={this.state.modalVisible}>
                      <SafeAreaView style={styles.SafeAreaViewStyle}>
                        <View style={styles.xStyle}>
                          <TouchableOpacity
                            onPress={() => {
                              this.setState({modalVisible: false});
                            }}>
                            <Text style={styles.xTextStyle}>X</Text>
                          </TouchableOpacity>
                        </View>
                        <View style={{flex: 2, alignItems: 'center'}}>
                          <Image
                            // source={this.state.document1Path}
                            source={{
                              uri: this.state.documents[this.state.whichImage],
                            }}
                            style={styles.imageStyle}
                            resizeMode="contain"
                          />
                        </View>
                      </SafeAreaView>
                    </Modal>
                  </View>
                </View>
              </View>
            </ScrollView>
          </KeyboardAvoidingView>
        </View>
        <View style={styles.editButtonViewStyle} pointerEvents="box-none">
          {this.state.Edit_or_Save ? (
            <TouchableOpacity
              style={styles.editButtonStyle}
              onPress={() => {
                this.setState({
                  editable: true,
                  Edit_or_Save: false,
                  editClick: true,
                });
              }}>
              <Edit />
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              style={styles.editButtonStyle}
              onPress={() => {
                this.setState({
                  editable: false,
                  Edit_or_Save: true,
                  editClick: false,
                });
              }}>
              <Save />
            </TouchableOpacity>
          )}
        </View>
      </View>
    );
  }
  ChooseDocument = (num) => {
    console.log('my doc', this.state.documents);
    if (typeof this.state.documents[num] === 'undefined') {
      console.log('number', num);
      let newArray = [...this.state.documents];
      ImagePicker.openPicker({
        width: 300,
        height: 400,
        cropping: true,
      }).then((image) => {
        console.log(image.path);
        newArray[num] = image.path;
        this.setState({documents: newArray});
        console.log('new Array', newArray);
      });
    } else {
      this.setState({whichImage: num});
      this.setState({modalVisible: true});
    }
  };
}
