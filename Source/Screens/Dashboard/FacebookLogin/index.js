import React from 'react';
import {View, StyleSheet, Text, Alert, Image} from 'react-native';
import {
  LoginButton,
  AccessToken,
  GraphRequest,
  GraphRequestManager,
} from 'react-native-fbsdk';

export default class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user_name: '',
      token: '',
      profile_pic: '',
    };
  }
  get_Response_Info = (error, result) => {
    if (error) {
      //Alert for the Error
      Alert.alert('Error fetching data: ' + error.toString());
    } else {
      //response alert
      alert(JSON.stringify(result));
      this.setState({user_name: 'Welcome' + ' ' + result.name});
      this.setState({token: 'User Token: ' + ' ' + result.id});
      this.setState({profile_pic: result.picture.data.url});
    }
  };

  onLogout = () => {
    //Clear the state after logout
    this.setState({user_name: null, token: null, profile_pic: null});
  };

  render() {
    return (
      <View style={styles.container}>
        {this.state.profile_pic ? (
          <Image
            source={{uri: this.state.profile_pic}}
            style={styles.imageStyle}
          />
        ) : null}
        <Text style={styles.text}> {this.state.user_name} </Text>
        <Text> {this.state.token} </Text>

        <LoginButton
          loginBehaviorAndroid="web_only"
          readPermissions={['public_profile']}
          onLoginFinished={(error, result) => {
            console.log('error--->> ', error);
            console.log('result--->> ', result);
            if (error) {
              Alert.alert(error);
              Alert.alert('login has error: ' + result.error);
            } else if (result.isCancelled) {
              Alert.alert('login is cancelled.');
            } else {
              AccessToken.getCurrentAccessToken().then((data) => {
                Alert.alert(data.accessToken.toString());
                console.log("token",data.accessToken.toString());
                const processRequest = new GraphRequest(
                  '/me?fields=name,picture.type(large)',
                  null,
                  this.get_Response_Info,
                );
                // Start the graph request.
                new GraphRequestManager().addRequest(processRequest).start();
              });
            }
          }}
          onLogoutFinished={this.onLogout}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
  },
  text: {
    fontSize: 20,
    color: '#000',
    textAlign: 'center',
    padding: 20,
  },
  imageStyle: {
    width: 200,
    height: 300,
    resizeMode: 'contain',
  },
});
