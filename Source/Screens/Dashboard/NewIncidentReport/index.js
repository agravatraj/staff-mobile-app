/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text, ScrollView, TextInput, Alert} from 'react-native';
import Header from '../../../Components/Header';
import styles from './styles';
import HeaderBig from '../../../Components/HeaderBig';
import {Dimens} from '../../../Utils/Dimens';
import {Plus, Send, Photo, Camera, DropdownArrow} from '../../../Utils/Svg';
import CustomeButton from '../../../Components/CustomeButton';
import {Colors} from '../../../Utils/Colors';
import Fonts from '../../../Utils/Fonts';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Modal from 'react-native-modal';
import PushPopButton from '../../../Components/PushPopButton';

export default class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      modalVisible: false,
      modal2Visible: false,
      modal3Visible: false,
      venueName: '',
      reportedBy: '',
      dateTimeOfInc: '',
      LocOfIncOther: '',
      seriousOther: '',
      refuseEntryReason: '',
      additionalIncDetail: '',
      myDateAndTime: '',
    };
  }
  componentDidMount() {
    var d = new Date();
    var myDate =
      d.getDate() +
      '/' +
      (d.getMonth() + 1) +
      '/' +
      d.getFullYear() +
      '  ' +
      d.getHours() +
      ':' +
      d.getMinutes();

    this.setState({myDateAndTime: myDate});
  }
  render() {
    console.log('render from new incident report called');
    return (
      <View style={styles.container}>
        <Header onPress={() => this.props.navigation.openDrawer()} />
        <View style={{flex: 1}}>
          <ScrollView
            bounces={false}
            contentContainerStyle={{flexGrow: 1}}
            keyboardShouldPersistTaps={'always'}>
            <View style={styles.container}>
              <HeaderBig
                title={
                  <Text>
                    New{' '}
                    <Text style={styles.boldtextStyle}>Incident reports</Text>
                  </Text>
                }
                backButton={false}
                backPress={() => {
                  this.props.navigation.goBack();
                }}
                cancelButton={true}
                cancelPress={() => {
                  this.props.navigation.goBack();
                }}
              />
              <View style={styles.container2}>
                <View style={styles.view1Style}>
                  <View style={styles.view1_1Style}>
                    <View style={styles.view1_2Style}>
                      <Text style={styles.heading1Style}>License Number</Text>
                      <Text style={styles.detail1Style}>0002343234</Text>
                    </View>
                    <View style={styles.view1_3Style}>
                      <Text style={styles.heading1Style}>Incident Number</Text>
                      <Text style={styles.detail1Style}>0002343234</Text>
                    </View>
                  </View>
                  <View style={{flexDirection: 'row', flex: 1}}>
                    <View style={styles.view1_2Style}>
                      <Text style={styles.heading1Style}>
                        Entry date & time
                      </Text>
                      <Text style={styles.detail1Style}>
                        {this.state.myDateAndTime}
                      </Text>
                    </View>
                    <View style={styles.view1_3Style}>
                      <Text style={styles.heading1Style}>Entered by</Text>
                      <Text style={styles.detail1Style}>Naser Al Zier</Text>
                    </View>
                  </View>
                </View>
                <View style={styles.card1}>
                  <View style={{flex: 1}}>
                    <Text style={styles.labelStyle}>Venue name</Text>
                  </View>
                  <View style={{flex: 1}}>
                    <TextInput
                      onChangeText={(txt) => {
                        this.setState({venueName: txt});
                      }}
                      // value={'The club house'}
                      style={styles.TextInputTextStyle}
                    />
                  </View>
                </View>
                <View style={styles.card1}>
                  <View style={{flex: 1}}>
                    <Text style={styles.labelStyle}>Reported By</Text>
                  </View>
                  <View style={{flex: 1}}>
                    <TextInput
                      onChangeText={(txt) => {
                        this.setState({reportedBy: txt});
                      }}
                      // value={'VJames miller'}
                      style={styles.TextInputTextStyle}
                    />
                  </View>
                </View>
                <View style={styles.card1}>
                  <View style={{flex: 1}}>
                    <Text style={styles.labelStyle}>Date/time of incident</Text>
                  </View>
                  <View style={{flex: 1}}>
                    <TextInput
                      onChangeText={(txt) => {
                        this.setState({dateTimeOfInc: txt});
                      }}
                      // value={'14/01/2020 21:30'}
                      style={styles.TextInputTextStyle}
                    />
                  </View>
                </View>
                <View style={styles.card1}>
                  <Text style={styles.labelStyle}>Location of incident</Text>
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({modalVisible: true});
                    }}>
                    <DropdownArrow />
                  </TouchableOpacity>
                  <Modal
                    backdropOpacity={0}
                    style={{
                      justifyContent: 'flex-end',
                      paddingBottom: 20,
                      paddingHorizontal: 30,
                    }}
                    isVisible={this.state.modalVisible}
                    onBackdropPress={() => {
                      this.setState({modalVisible: false});
                    }}>
                    <View style={styles.modalView}>
                      <View style={styles.modalView1_1}>
                        <PushPopButton title="Refuse entry" />
                        <PushPopButton title="Refuse service" />
                        <PushPopButton title="Theft" />
                        <PushPopButton title="Malicious damage" />
                        <PushPopButton title="Complaint" />
                        <PushPopButton title="Minors" />
                        <PushPopButton title="Self exclusion" />
                        <PushPopButton title="Gaming" />
                      </View>
                      <View style={styles.cancelViewStyle}>
                        <Text
                          style={styles.cancel}
                          onPress={() => {
                            this.setState({modalVisible: false});
                          }}>
                          Cancel
                        </Text>
                        <Text style={styles.add}>Add</Text>
                      </View>
                    </View>
                  </Modal>
                </View>
                <View style={styles.card1}>
                  <View style={{flex: 1}}>
                    <Text style={styles.labelStyle}>
                      Location of incident (others)
                    </Text>
                  </View>
                  <View style={{flex: 1}}>
                    <TextInput
                      onChangeText={(txt) => {
                        this.setState({LocOfIncOther: txt});
                      }}
                      style={styles.TextInputTextStyle}
                    />
                  </View>
                </View>
                <View style={styles.card1}>
                  <Text style={styles.labelStyle}>Incident details</Text>
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({modal2Visible: true});
                    }}>
                    <DropdownArrow />
                  </TouchableOpacity>
                  <Modal
                    backdropOpacity={0}
                    style={{
                      justifyContent: 'flex-end',
                      paddingBottom: 20,
                      paddingHorizontal: 30,
                    }}
                    isVisible={this.state.modal2Visible}
                    onBackdropPress={() => {
                      this.setState({
                        modalVisible: false,
                        modal2Visible: false,
                      });
                    }}>
                    <View style={styles.modalView}>
                      <View style={styles.modalView1_1}>
                        <PushPopButton title="Insufficient ID" />
                        <PushPopButton title="Minors" />
                        <PushPopButton title="Suspected intoxication" />
                      </View>
                      <View style={styles.cancelViewStyle}>
                        <Text
                          style={styles.cancel}
                          onPress={() => {
                            this.setState({modal2Visible: false});
                          }}>
                          Cancel
                        </Text>
                        <Text style={styles.add}>Add</Text>
                      </View>
                    </View>
                  </Modal>
                </View>
                <View style={styles.card1}>
                  <View style={{flex: 1}}>
                    <Text style={styles.labelStyle}>Serious (other)</Text>
                  </View>
                  <View style={{flex: 1}}>
                    <TextInput
                      onChangeText={(txt) => {
                        this.setState({seriousOther: txt});
                      }}
                      style={styles.TextInputTextStyle}
                    />
                  </View>
                </View>
                <View style={styles.card1}>
                  <View style={{flex: 1}}>
                    <Text style={styles.labelStyle}>Refuse entry reason</Text>
                  </View>
                  <View style={{flex: 1}}>
                    <TextInput
                      onChangeText={(txt) => {
                        this.setState({refuseEntryReason: txt});
                      }}
                      style={styles.TextInputTextStyle}
                    />
                  </View>
                </View>
                <View style={styles.card1}>
                  <View style={{flex: 1}}>
                    <Text style={styles.labelStyle}>
                      Additional incident details
                    </Text>
                  </View>
                  <View style={{flex: 1}}>
                    <TextInput
                      onChangeText={(txt) => {
                        this.setState({additionalIncDetail: txt});
                      }}
                      style={styles.TextInputTextStyle}
                    />
                  </View>
                </View>
                <View style={styles.card1}>
                  <Text style={styles.labelStyle}>Asked to leave reason</Text>
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({modal3Visible: true});
                    }}>
                    <DropdownArrow />
                  </TouchableOpacity>
                  <Modal
                    backdropOpacity={0}
                    style={{
                      justifyContent: 'flex-end',
                      paddingBottom: 20,
                      paddingHorizontal: 30,
                    }}
                    isVisible={this.state.modal3Visible}
                    onBackdropPress={() => {
                      this.setState({modal3Visible: false});
                    }}>
                    <View style={styles.modalView}>
                      <View style={styles.modalView1_1}>
                        <PushPopButton title="Approaching intoxication" />
                        <PushPopButton title="Suspected intoxication" />
                        <PushPopButton title="Violent" />
                        <PushPopButton title="Anti-social" />
                        <PushPopButton title="Illicit substances" />
                        <PushPopButton title="Insufficient ID" />
                        <PushPopButton title="Minor" />
                        <PushPopButton title="Smoking in no-smoking area" />
                      </View>
                      <View style={styles.cancelViewStyle}>
                        <Text
                          style={styles.cancel}
                          onPress={() => {
                            this.setState({modal3Visible: false});
                          }}>
                          Cancel
                        </Text>
                        <Text style={styles.add}>Add</Text>
                      </View>
                    </View>
                  </Modal>
                </View>

                <View style={{alignSelf: 'flex-end', marginVertical: 20}}>
                  <CustomeButton
                    onPress={() => {
                      this.props.navigation.navigate(
                        'NewIncidentReportPage2Screen',
                      );

                      // this.CheckValidation() === true
                      //   ? this.props.navigation.navigate(
                      //       'NewIncidentReportPage2Screen',
                      //     )
                      //   : null;
                      // console.log('venue name=', this.state.venueName);
                      // console.log('reporder by=', this.state.reportedBy);
                      // console.log(
                      //   'Date time of inc=',
                      //   this.state.dateTimeOfInc,
                      // );
                      // console.log('loc of inc=', this.state.LocOfIncOther);
                      // console.log('serious=', this.state.seriousOther);
                      // console.log(
                      //   'refuse entry reson=',
                      //   this.state.refuseEntryReason,
                      // );
                      // console.log(
                      //   'additional inc=',
                      //   this.state.additionalIncDetail,
                      // );
                    }}
                    title="Next"
                    backgroundColor={Colors.orange}
                    width={130}
                    height={40}
                    borderRadius={15}
                    color={Colors.white}
                    fontSize={16}
                    fontFamily={Fonts.HelveticaNeueMedium}
                  />
                </View>
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
  CheckValidation = () => {
    if (this.state.venueName === '') {
      Alert.alert('enter vanue name');
      return false;
    }
    if (this.state.reportedBy === '') {
      Alert.alert('enter reported by');
      return false;
    }
    if (this.state.dateTimeOfInc === '') {
      Alert.alert('enter date time of incident');
      return false;
    }
    if (this.state.LocOfIncOther === '') {
      Alert.alert('enter location of incident ');
      return false;
    }
    if (this.state.seriousOther === '') {
      Alert.alert('enter serious');
      return false;
    }
    if (this.state.refuseEntryReason === '') {
      Alert.alert('enter refuse entry reason');
      return false;
    }
    if (this.state.additionalIncDetail === '') {
      Alert.alert('enter additional Incident Detail');
      return false;
    }
    return true;
  };
}
