/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text, ScrollView, TextInput, Alert} from 'react-native';
import Header from '../../../Components/Header';
import styles from './styles';
import HeaderBig from '../../../Components/HeaderBig';
import {DropdownArrow} from '../../../Utils/Svg';
import CustomeButton from '../../../Components/CustomeButton';
import {Colors} from '../../../Utils/Colors';
import Fonts from '../../../Utils/Fonts';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Modal from 'react-native-modal';
import PushPopButton from '../../../Components/PushPopButton';
import {AccordionList} from 'accordion-collapse-react-native';

export default class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      modal1Visible: false,
      modal2Visible: false,
      persons1Description: '',
      name: '',
      phoneNumber: '',
      Persons2Description: '',
      list: [
        {
          id: 1,
          title: 'Person of interest 1',
          name: 'Agravat Raj',
        },
        {
          id: 2,
          title: 'Person of interest 2',
          name: 'Baraiya mitesh',
        },
        {
          id: 3,
          title: 'Person of interest 3',
          name: 'jani jaydeep',
        },
      ],
    };
  }
  _head(item) {
    return (
      <View style={styles.headerView}>
        <Text style={styles.headerText}>{item.title}</Text>
      </View>
    );
  }

  _body = (item) => {
    return (
      <View style={{padding: 10}}>
        <View style={styles.card1}>
          <View style={{flex: 1}}>
            <Text style={styles.labelStyle}>Name</Text>
          </View>
          <View style={{flex: 1}}>
            <TextInput
              onChangeText={(txt) => {
                this.setState({name: txt});
              }}
              style={styles.TextInputTextStyle}
            />
          </View>
        </View>
        <View style={styles.card1}>
          <View style={{flex: 1}}>
            <Text style={styles.labelStyle}>Phone number</Text>
          </View>
          <View style={{flex: 1}}>
            <TextInput
              onChangeText={(txt) => {
                this.setState({phoneNumber: txt});
              }}
              style={styles.TextInputTextStyle}
            />
          </View>
        </View>
        <View style={styles.card1}>
          <Text style={styles.labelStyle}>Age</Text>
          <TouchableOpacity
            onPress={() => {
              this.setState({modal1Visible: true});
            }}>
            <DropdownArrow />
          </TouchableOpacity>
          <Modal
            isVisible={this.state.modal1Visible}
            backdropOpacity={0}
            style={{
              justifyContent: 'flex-end',
              paddingBottom: 60,
              paddingHorizontal: 30,
            }}
            onBackdropPress={() => {
              this.setState({
                modal1Visible: false,
              });
            }}>
            <View style={styles.modalView}>
              <View style={styles.modalView1_1}>
                <PushPopButton title="Under 18" />
                <PushPopButton title="18-25" />
                <PushPopButton title="26-34" />
                <PushPopButton title="35+" />
              </View>
              <View style={styles.cancelViewStyle}>
                <Text
                  style={styles.cancel}
                  onPress={() => {
                    this.setState({modal1Visible: false});
                  }}>
                  Cancel
                </Text>
                <Text style={styles.add}>Add</Text>
              </View>
            </View>
          </Modal>
        </View>
        <View style={styles.card1}>
          <Text style={styles.labelStyle}>Gender</Text>
          <TouchableOpacity
            onPress={() => {
              this.setState({modal2Visible: true});
            }}>
            <DropdownArrow />
          </TouchableOpacity>
          <Modal
            isVisible={this.state.modal2Visible}
            backdropOpacity={0}
            style={{
              justifyContent: 'flex-end',
              paddingBottom: 60,
              paddingHorizontal: 30,
            }}
            onBackdropPress={() => {
              this.setState({
                modal2Visible: false,
              });
            }}>
            <View style={styles.modalView}>
              <View style={styles.modalView1_1}>
                <PushPopButton title="Male" />
                <PushPopButton title="Female" />
                <PushPopButton title="Other" />
              </View>
              <View style={styles.cancelViewStyle}>
                <Text
                  style={styles.cancel}
                  onPress={() => {
                    this.setState({modal2Visible: false});
                  }}>
                  Cancel
                </Text>
                <Text style={styles.add}>Add</Text>
              </View>
            </View>
          </Modal>
        </View>
        <View
          style={[
            styles.card1,
            {flexDirection: 'column', alignItems: 'flex-start', height: null},
          ]}>
          <Text style={styles.labelStyle}>Person(s) Description</Text>
          <View style={{paddingLeft: 30, paddingTop: 10}}>
            <TextInput
              placeholder=""
              multiline={true}
              style={styles.TextInputTextStyle}
              onChangeText={(txt) => {
                this.setState({Persons2Description: txt});
              }}
            />
          </View>
        </View>
      </View>
    );
  };
  render() {
    return (
      <View style={styles.container}>
        <Header onPress={() => this.props.navigation.openDrawer()} />
        <View style={{flex: 1}}>
          <ScrollView
            bounces={false}
            contentContainerStyle={{flexGrow: 1}}
            keyboardShouldPersistTaps="always">
            <View style={styles.container}>
              <HeaderBig
                title={
                  <Text>
                    New{' '}
                    <Text style={styles.boldtextStyle}>Incident reports</Text>
                  </Text>
                }
                backButton={true}
                backPress={() => {
                  this.props.navigation.goBack();
                }}
              />
              <View style={styles.container2}>
                <View
                  style={[
                    styles.card1,
                    {
                      padding: 0,
                      paddingVertical: 0,
                      height: 40,
                      marginHorizontal: 10,
                      paddingLeft: 0,
                    },
                  ]}>
                  <View style={styles.nofpiView}>
                    <Text style={styles.labelStyle}>
                      No of Persons Involved
                    </Text>
                  </View>
                  <View style={styles.nofpiView2}>
                    <Text>3</Text>
                  </View>
                </View>
                <View
                  style={[
                    styles.card1,
                    {
                      flexDirection: 'column',
                      marginHorizontal: 10,
                      alignItems: 'flex-start',
                      height: null,
                    },
                  ]}>
                  <Text style={styles.labelStyle}>Person(s) Description</Text>
                  <View style={{paddingLeft: 30, paddingTop: 10}}>
                    <TextInput
                      placeholder=""
                      multiline={true}
                      style={styles.TextInputTextStyle}
                      onChangeText={(txt) => {
                        this.setState({persons1Description: txt});
                      }}
                    />
                  </View>
                </View>
                <View>
                  <AccordionList
                    list={this.state.list}
                    header={this._head}
                    body={this._body}
                    keyExtractor={(item) => `${item.id}`}
                  />
                </View>
                <View
                  style={{
                    alignSelf: 'flex-end',
                    marginVertical: 20,
                    marginRight: 10,
                  }}>
                  <CustomeButton
                    onPress={() => {
                      // this.CheckValidation() === true
                      //   ? this.props.navigation.navigate(
                      //       'NewIncidentReportPage3Screen',
                      //     )
                      //   : null;
                      this.props.navigation.navigate(
                        'NewIncidentReportPage3Screen',
                      );
                    }}
                    title="Next"
                    backgroundColor={Colors.orange}
                    width={130}
                    height={40}
                    borderRadius={15}
                    color={Colors.white}
                    fontSize={16}
                    fontFamily={Fonts.HelveticaNeueMedium}
                  />
                </View>
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
  CheckValidation = () => {
    if (this.state.persons1Description === '') {
      Alert.alert('enter persons description');
      return false;
    }
    if (this.state.name === '') {
      Alert.alert('enter name');
      return false;
    }
    if (this.state.phoneNumber === '') {
      Alert.alert('enter phone number');
      return false;
    }
    if (this.state.Persons2Description === '') {
      Alert.alert('enter persons 2 description ');
      return false;
    }
    return true;
  };
}
