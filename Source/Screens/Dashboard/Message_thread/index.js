/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text, ScrollView, FlatList, RefreshControl} from 'react-native';
import Header from '../../../Components/Header';
import styles from './styles';
import HeaderBig from '../../../Components/HeaderBig';
import {Colors} from '../../../Utils/Colors';
import {Dimens} from '../../../Utils/Dimens';
import {Usericon, MessageButton} from '../../../Utils/Svg';

export default class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
    };
  }
  render() {
    const {personName, message} = this.props.route.params;
    return (
      <View style={styles.container}>
        <Header onPress={() => this.props.navigation.openDrawer()} />
        <View style={{flex: 1}}>
          <ScrollView bounces={false} contentContainerStyle={{flexGrow: 1}}>
            <View style={styles.container}>
              <HeaderBig
                title={<Text style={styles.boldtextStyle}>{personName}</Text>}
                backButton={true}
                backPress={() => {
                  this.props.navigation.goBack();
                }}
              />
              <View style={styles.container2}>
                <Text>{message}</Text>
                {/* <FlatList
                  data={Mydata}
                  renderItem={this.renderItem}
                  keyExtractor={(item) => item.id + ''}
                  showsVerticalScrollIndicator={false}
                  style={{flex: 1}}
                  contentContainerStyle={{flexGrow: 1}}
                  refreshControl={
                    <RefreshControl
                      colors={['#9Bd35A', '#689F38']}
                      refreshing={this.state.refreshing}
                      onRefresh={() => {
                        this.setState({refreshing: false});
                      }}
                    />
                  }
                /> */}
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
  renderItem = ({item}) => (
    <View style={styles.card1}>
      <Text>hi</Text>
    </View>
  );
}
