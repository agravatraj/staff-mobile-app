import React from 'react';
import {View, Text, Alert, TextInput} from 'react-native';
import Header from '../../../Components/Header';
import styles from './styles';
import HeaderBig from '../../../Components/HeaderBig';
import CustomeButton from '../../../Components/CustomeButton';
import {Colors} from '../../../Utils/Colors';
import Fonts from '../../../Utils/Fonts';
import {Dimens} from '../../../Utils/Dimens';
import Modal from 'react-native-modal';
export default class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={styles.container}>
        <Header onPress={() => this.props.navigation.openDrawer()} />
        <HeaderBig
          title="Welcome Back,"
          title2="Naser AL Zier"
          title3="upcomming Shifts"
        />
        <View style={styles.container2}>
          <View style={styles.card1}>
            <Text style={styles.card1titleStyle}>
              Shift requiring confirmation
            </Text>
            <View style={styles.card1_1}>
              <View style={styles.innerview1Style}>
                <Text style={styles.number}>4</Text>
                <Text style={styles.shift}>my Shift</Text>
              </View>
              <View style={styles.innerview1Style}>
                <Text style={styles.number}>1</Text>
                <Text style={styles.shift}>Open Shift</Text>
              </View>
            </View>
          </View>

          <View style={styles.card2}>
            <Text style={styles.card1titleStyle}>Upcoming shift</Text>
            <View style={styles.card2_2}>
              <Text style={styles.customerText}>Customer1</Text>
              <View style={styles.View2style}>
                <View style={styles.smallView1}>
                  <Text style={styles.siteText}>Site 1</Text>
                </View>
                <View style={styles.smallView2}>
                  <Text style={styles.siteText}>Area 1</Text>
                </View>
              </View>
              <View style={styles.View2style}>
                <Text style={styles.customerText}>Monday</Text>
                <Text style={styles.dateTimeStyle}>10/08/2020</Text>
                <Text style={styles.dateTimeStyle}>09:00am - 1:00pm</Text>
              </View>
              <View style={styles.buttonView}>
                <CustomeButton
                  title="View button"
                  color={Colors.black}
                  fontFamily={Fonts.HelveticaNeue}
                  fontSize={Dimens.dimen_14}
                  borderWidth={1}
                  borderColor={Colors.borderColor}
                  borderRadius={Dimens.dimen_10}
                  height={Dimens.dimen_35}
                  width={Dimens.dimen_150}
                />
                <CustomeButton
                  title="Clock in"
                  color={Colors.clockButtonTextColor}
                  fontFamily={Fonts.helveticaNeueBold}
                  fontSize={Dimens.dimen_14}
                  borderWidth={1}
                  borderColor={Colors.borderColor}
                  backgroundColor={Colors.clockButtonColor}
                  borderRadius={Dimens.dimen_10}
                  height={Dimens.dimen_35}
                  width={Dimens.dimen_150}
                />
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
