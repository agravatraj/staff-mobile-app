/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  FlatList,
  Alert,
  TextInput,
} from 'react-native';
import Header from '../../../Components/Header';
import HeaderBig from '../../../Components/HeaderBig';
import styles from './styles';
import {Plus2} from '../../../Utils/Svg';
import {Colors} from '../../../Utils/Colors';
import Fonts from '../../../Utils/Fonts';
import {Calendar, Agenda} from 'react-native-calendars';
import CustomeButton from '../../../Components/CustomeButton';

export default class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={styles.container}>
        <Header onPress={() => this.props.navigation.openDrawer()} />
        <ScrollView bounces={false} contentContainerStyle={{flexGrow: 1}}>
          <View style={styles.container}>
            <HeaderBig
              title={
                <Text>
                  New <Text style={styles.boldtextStyle}>Request</Text>
                </Text>
              }
              backButton={true}
              backPress={() => {
                this.props.navigation.goBack();
              }}
            />
            <View style={styles.container2}>
              <Calendar />
              <Text style={styles.heading}>Leave type</Text>
              <View style={styles.card1}>
                <TextInput placeholder="" />
              </View>
              <Text style={styles.heading}>Customer Site</Text>
              <View style={styles.card1}>
                <TextInput placeholder="" />
              </View>
              <Text style={styles.heading}>Supervisor to notify</Text>
              <View style={styles.card1}>
                <TextInput placeholder="" />
              </View>
              <Text style={styles.heading}>Comments/Notes</Text>
              <View style={styles.card1}>
                <TextInput placeholder="" />
              </View>
              <View style={{alignSelf: 'flex-end', marginVertical: 20}}>
                <CustomeButton
                  title="Submit"
                  backgroundColor={Colors.orange}
                  width={130}
                  height={40}
                  borderRadius={15}
                  color={Colors.white}
                  fontSize={16}
                  fontFamily={Fonts.HelveticaNeueMedium}
                />
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}
