/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  SectionList,
  ScrollView,
  Platform,
  SafeAreaView,
  TextInput,
  KeyboardAvoidingView,
  Alert,
  TouchableHighlight,
} from 'react-native';
import Modal from 'react-native-modal';
import styles from './styles';
import Header from '../../../Components/Header';
import HeaderBig from '../../../Components/HeaderBig';
import {ListDots, Map} from '../../../Utils/Svg';
import {Dimens} from '../../../Utils/Dimens';
import CustomeButton from '../../../Components/CustomeButton';
import Tooltip from 'react-native-walkthrough-tooltip';
import {
  MenuProvider,
  MenuOption,
  MenuOptions,
  Menu,
  MenuTrigger,
  renderers,
} from 'react-native-popup-menu';
import Popover from 'react-native-popover-view';

import {Colors} from '../../../Utils/Colors';
import Fonts from '../../../Utils/Fonts';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
// const {ContextMenu, SlideInMenu, Popover} = renderers;

const optionsStyles = {
  optionsContainer: {
    backgroundColor: Colors.white,
    padding: 15,
    borderRadius: 15,
  },
};

export default class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      opened: false,
      ModalClockInVisible: false,
      ModalClockOutVisible: false,
      clockInBtn: false,
      clockOutBtn: false,
      toolTipVisible: false,
    };
  }
  _toolTip() {
    return (
      <Tooltip
        isVisible={this.state.toolTipVisible}
        content={
          <Text
            onPress={() => {
              Alert.alert('hi');
            }}>
            Check this out!
          </Text>
        }
        placement="left"
        onClose={() => this.setState({toolTipVisible: false})}>
        <TouchableOpacity
          // ref={touchable}
          // onPress={}
          style={{paddingHorizontal: 10, paddingVertical: 5}}>
          <ListDots stroke={Colors.white} />
        </TouchableOpacity>
      </Tooltip>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <Header
          onPress={() => {
            this.props.navigation.openDrawer();
          }}
        />
        <ScrollView bounces={false} contentContainerStyle={{flexGrow: 1}}>
          <HeaderBig
            title={<Text>Here’s your</Text>}
            title2="Weekly schedule"
            backButton={true}
            backPress={() => this.props.navigation.goBack()}
            menuButton={true}
            menuPress={() => {
              this.setState({toolTipVisible: true});
            }}
            tooltipView={this._toolTip.bind(this)}
          />
          <View style={styles.container2}>
            <View style={{flex: 1}}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                  paddingHorizontal: 15,
                  paddingVertical: 5,
                }}>
                <View style={{flex: 1, paddingHorizontal: 10}}>
                  <CustomeButton
                    title="Clock in"
                    backgroundColor={
                      this.state.clockInBtn
                        ? Colors.clockButtonColor
                        : Colors.clockButtonBackgroundColor
                    }
                    height={30}
                    borderRadius={10}
                    onPress={() => {
                      this.setState({
                        ModalClockInVisible: true,
                        clockInBtn: true,
                        clockOutBtn: false,
                      });
                    }}
                  />
                </View>
                <View style={{flex: 1, paddingHorizontal: 10}}>
                  <CustomeButton
                    title="Clock out"
                    backgroundColor={
                      this.state.clockOutBtn
                        ? Colors.clockButtonColor
                        : Colors.clockButtonBackgroundColor
                    }
                    height={30}
                    borderRadius={10}
                    onPress={() => {
                      this.setState({
                        ModalClockOutVisible: true,
                        clockInBtn: false,
                        clockOutBtn: true,
                      });
                    }}
                  />
                </View>
              </View>

              <View style={styles.cardStyle}>
                <View style={styles.view1}>
                  <Text style={styles.header}>{'Customer 1'}</Text>
                  <TouchableOpacity
                    style={{paddingHorizontal: 10}}
                    onPress={() => {
                      this.setState({opened: true});
                    }}
                  />
                </View>
                <View style={styles.view2}>
                  <View style={styles.site1View}>
                    <Text style={styles.siteStyle}>{'Site 1'}</Text>
                  </View>
                  <View style={styles.area1View}>
                    <Text style={styles.areaStyle}>{'Area 1'}</Text>
                  </View>
                </View>
                <View style={styles.view3}>
                  <View style={{flex: 1}}>
                    <Text style={styles.address}>
                      {' '}
                      {
                        'Edwin Flack Ave, Sydney Olympic Park NSW 2127, Australia'
                      }{' '}
                    </Text>
                  </View>
                  <TouchableOpacity
                    style={{paddingHorizontal: 10, marginLeft: 15}}>
                    <Map />
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.cardStyle}>
                <View>
                  <Text style={styles.datetimeLabel}>Date</Text>
                  <Text style={styles.datetime}>Monday, 22/12/2020</Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    marginTop: Platform.OS === 'ios' ? 5 : 0,
                  }}>
                  <View style={{flex: 1}}>
                    <Text style={styles.datetimeLabel}>Start Time</Text>
                    <Text style={styles.datetime}>09:00 am</Text>
                  </View>
                  <View style={{flex: 1}}>
                    <View
                      style={{
                        borderRadius: 15,
                        padding: 5,
                        paddingLeft: 10,
                      }}>
                      <Text style={styles.datetimeLabel}>Finish Time</Text>
                      <Text style={styles.datetime}>05:00 pm</Text>
                    </View>
                  </View>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    marginTop: Platform.OS === 'ios' ? 5 : 0,
                  }}>
                  <View style={{flex: 1}}>
                    <Text style={styles.datetimeLabel}>Breaks</Text>
                    <Text style={styles.datetime}>30 minutes</Text>
                  </View>
                  <View style={{flex: 1}}>
                    <View
                      style={{
                        backgroundColor: Colors.lightgrey,
                        borderRadius: 15,
                        padding: 5,
                        paddingLeft: 10,
                      }}>
                      <Text style={styles.datetimeLabel}>Total Time</Text>
                      <Text style={styles.datetime}>8hrs 30mins</Text>
                    </View>
                  </View>
                </View>
              </View>
              <View style={styles.cardStyle}>
                <Text style={styles.datetimeLabel}>Notes/comments</Text>
                <Text style={styles.address}>
                  Please be on time and wear standard black shirt and shoes
                </Text>
              </View>
            </View>
          </View>
          {this.ModalClockIn()}
          {this.ModalClockOut()}
        </ScrollView>
      </View>
    );
  }
  // MenuComponent = () => (
  //   <Menu
  //     opened={this.state.opened}
  //     renderer={Popover}
  //     onBackdropPress={() => this.setState({opened: false})}
  //     style={{backgroundColor: 'red'}}>
  //     <MenuTrigger />
  //     <MenuOptions>
  //       <MenuOption onSelect={() => alert('Save')} text="View Details" />
  //       <MenuOption onSelect={() => alert('Delete')}>
  //         <Text>Confirm shift</Text>
  //       </MenuOption>
  //       <MenuOption onSelect={() => alert('Not called')} text="Decline shift" />
  //     </MenuOptions>
  //   </Menu>
  // );

  ModalClockIn = () => (
    <KeyboardAvoidingView style={{flex: 1}} behavior={'position'}>
      <Modal
        onBackdropPress={() => {
          this.setState({ModalClockInVisible: false});
        }}
        isVisible={this.state.ModalClockInVisible}
        transparent={true}
        style={{justifyContent: 'center', flex: 1}}>
        <SafeAreaView style={styles.modalStyle}>
          <Text style={styles.clockinText}>Clock in</Text>
          <Text style={styles.clockinTimeText}>Clock in time</Text>
          <Text style={styles.timeText}>09:10 am</Text>
          <Text style={styles.locationText}>Clock in location</Text>
          <View
            style={{
              overflow: 'hidden',
              flex: 2,
              backgroundColor: Colors.light,
              borderRadius: 25,
              marginVertical: Dimens.dimen_5,
            }}>
            <MapView
              style={{flex: 1}}
              provider={PROVIDER_GOOGLE}
              region={{
                latitude: 37.78825,
                longitude: -122.4324,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
              }}
            />
          </View>
          <View style={{flex: 1}}>
            <Text style={styles.clockinTimeText}>
              Current distance from site
            </Text>
            <Text style={styles.timeText}>0.7km (15.2 meter accuracy)</Text>
          </View>
          <View style={{flex: 1}}>
            <Text style={styles.clockinTimeText}>Scan Site QR code</Text>
            <Text style={styles.italicStyle}>Click here to scan</Text>
          </View>
          <View style={{flex: 1}}>
            <Text style={styles.clockinTimeText}>
              Biometric recognition validation
            </Text>
            <Text style={styles.italicStyle}>Facial recognition validated</Text>
          </View>
          <View style={{}}>
            <Text style={styles.clockinTimeText}>Comments</Text>
            <View style={[styles.card2Style, {height: 50}]}>
              {/* <TextInput placeholder="enter comments" /> */}
              <Text />
              <Text />
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              paddingHorizontal: 10,
              alignItems: 'center',
              paddingVertical: Platform.OS === 'ios' ? 10 : 10,
            }}>
            <Text
              style={{
                color: Colors.orange,
                fontSize: 16,
                fontFamily: Fonts.helveticaNeueBold,
              }}
              onPress={() => {
                this.setState({ModalClockInVisible: false});
              }}>
              Cancel
            </Text>
            <Text
              style={{
                color: Colors.lightgreen,
                fontSize: 16,
                fontFamily: Fonts.helveticaNeueBold,
              }}>
              Confirm
            </Text>
          </View>
        </SafeAreaView>
      </Modal>
    </KeyboardAvoidingView>
  );
  ModalClockOut = () => (
    <KeyboardAvoidingView style={{flex: 1}} behavior={'position'}>
      <Modal
        onBackdropPress={() => {
          this.setState({ModalClockOutVisible: false});
        }}
        isVisible={this.state.ModalClockOutVisible}
        transparent={true}
        style={{justifyContent: 'center', flex: 1}}>
        <SafeAreaView style={styles.modalStyle}>
          <Text style={styles.clockinText}>Clock Out</Text>
          <Text style={styles.clockinTimeText}>Clock Out time</Text>
          <Text style={styles.timeText}>09:10 am</Text>
          <Text style={styles.locationText}>Clock Out location</Text>
          <View
            style={{
              overflow: 'hidden',
              flex: 2,
              backgroundColor: Colors.light,
              borderRadius: 25,
              marginVertical: Dimens.dimen_5,
            }}>
            <MapView
              style={{flex: 1}}
              provider={PROVIDER_GOOGLE}
              region={{
                latitude: 37.78825,
                longitude: -122.4324,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
              }}
            />
          </View>
          <View style={{flex: 1}}>
            <Text style={styles.clockinTimeText}>
              Current distance from site
            </Text>
            <Text style={styles.timeText}>0.7km (15.2 meter accuracy)</Text>
          </View>
          <View style={{flex: 1}}>
            <Text style={styles.clockinTimeText}>
              Biometric recognition validation
            </Text>
            <Text style={styles.italicStyle}>Facial recognition validated</Text>
          </View>
          <View style={{flex: 1}}>
            <Text style={styles.clockinTimeText}>Comments</Text>
            <View style={styles.card2Style}>
              {/* <TextInput placeholder="enter comments" /> */}
              <Text />
              <Text />
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              paddingHorizontal: 10,
              alignItems: 'center',
              paddingVertical: Platform.OS === 'ios' ? 0 : 10,
            }}>
            <Text
              style={{
                color: Colors.orange,
                fontSize: 16,
                fontFamily: Fonts.helveticaNeueBold,
              }}
              onPress={() => {
                this.setState({ModalClockOutVisible: false});
              }}>
              Cancel
            </Text>
            <Text
              style={{
                color: Colors.lightgreen,
                fontSize: 16,
                fontFamily: Fonts.helveticaNeueBold,
              }}>
              Confirm
            </Text>
          </View>
        </SafeAreaView>
      </Modal>
    </KeyboardAvoidingView>
  );
}
