import {Platform, StyleSheet} from 'react-native';
import {Colors} from '../../../Utils/Colors';
import fonts from '../../../Utils/Fonts';
import {Dimens} from '../../../Utils/Dimens';
import Fonts from '../../../Utils/Fonts';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  container2: {
    flex: 1,
    paddingTop: Dimens.dimen_15,
  },
  weekStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingVertical: Dimens.dimen_10,
  },
  lastNextweek: {
    fontSize: Dimens.dimen_14,
    fontFamily: fonts.SourceSansProRegular,
    color: Colors.lightblue,
  },
  thisweek: {
    fontSize: Dimens.dimen_14,
    fontFamily: fonts.SourceSansProBold,
    color: Colors.lightblue,
  },
  cardStyle: {
    backgroundColor: Colors.white,
    marginVertical: Dimens.dimen_5,
    elevation: Dimens.dimen_5,
    borderRadius: Dimens.dimen_15,
    paddingHorizontal: Dimens.dimen_20,
    paddingVertical: Dimens.dimen_15,
    shadowColor: Colors.black,
    shadowOffset: {width: 0, height: Dimens.dimen_3},
    shadowOpacity: 0.5,
    shadowRadius: Dimens.dimen_5,
    marginHorizontal: Dimens.dimen_15,
    marginBottom: Dimens.dimen_10,
  },
  card2Style: {
    backgroundColor: Colors.white,
    marginVertical: Dimens.dimen_5,
    elevation: Dimens.dimen_5,
    borderRadius: Dimens.dimen_15,
    paddingHorizontal: Dimens.dimen_20,
    shadowColor: Colors.black,
    shadowOffset: {width: 0, height: Dimens.dimen_3},
    shadowOpacity: 0.5,
    shadowRadius: Dimens.dimen_5,
    paddingVertical: Platform.OS === 'ios' ? Dimens.dimen_10 : 0,
  },
  view1: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  view2: {
    flexDirection: 'row',
    marginTop: Dimens.dimen_5,
  },
  siteStyle: {
    paddingHorizontal: Dimens.dimen_5,
    fontSize: Dimens.dimen_10,
    fontFamily: Fonts.SourceSansProBold,
    color: Colors.white,
    alignSelf: 'center',
  },
  areaStyle: {
    paddingHorizontal: Dimens.dimen_5,
    fontFamily: Fonts.SourceSansProBold,
    color: Colors.white,
    alignSelf: 'center',
    fontSize: Dimens.dimen_10,
  },
  shiftTextStyle: {
    paddingHorizontal: Dimens.dimen_5,
    fontFamily: Fonts.SourceSansProRegular,
    color: Colors.white,
    alignSelf: 'center',
    fontSize: Dimens.dimen_10,
  },
  shiftConfirm: {
    backgroundColor: Colors.pinkDark,
    borderRadius: Dimens.dimen_3,
  },
  site1View: {
    backgroundColor: Colors.lightgreen,
    borderRadius: Dimens.dimen_3,
    marginRight: Dimens.dimen_10,
  },
  area1View: {
    backgroundColor: Colors.sky,
    borderRadius: Dimens.dimen_3,
    marginRight: Dimens.dimen_10,
  },
  view3: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: Dimens.dimen_3,
  },
  day: {
    fontSize: Dimens.dimen_16,
    fontFamily: Fonts.helveticaNeueBold,
  },
  datetimeLabel: {
    fontSize: Dimens.dimen_16,
    fontFamily: Fonts.HelveticaNeueMedium,
    color: Colors.lightblue,
  },
  datetime: {
    fontSize: Dimens.dimen_16,
    fontFamily: Fonts.HelveticaNeue,
  },
  header: {
    fontSize: Dimens.dimen_16,
    fontFamily: Fonts.helveticaNeueBold,
  },
  SectionListView: {
    flex: 1,
  },
  address: {
    fontFamily: fonts.HelveticaNeueLight,
    fontSize: Dimens.dimen_16,
    marginTop: Platform.OS == 'ios' ? Dimens.dimen_5 : 0,
  },
  modalStyle: {
    flex: 1,
    marginVertical: Platform.OS === 'ios' ? Dimens.dimen_100 : Dimens.dimen_50,
    marginHorizontal: Dimens.dimen_25,
    backgroundColor: Colors.white,
    elevation: Dimens.dimen_5,
    borderRadius: Dimens.dimen_25,
    paddingHorizontal: Dimens.dimen_20,
    paddingVertical: Dimens.dimen_15,
    shadowColor: Colors.black,
    shadowOffset: {width: 0, height: Dimens.dimen_3},
    shadowOpacity: 0.5,
    shadowRadius: Dimens.dimen_5,
    //justifyContent: 'space-between',
  },
  clockinText: {
    alignSelf: 'center',
    fontSize: Dimens.dimen_20,
    fontFamily: Fonts.helveticaNeueBold,
  },
  clockinTimeText: {
    fontFamily: fonts.HelveticaNeueMedium,
    color: Colors.lightblue,
    fontSize: Dimens.dimen_16,
  },
  timeText: {
    fontFamily: fonts.HelveticaNeue,
    fontSize: Dimens.dimen_14,
    marginTop: Platform.OS === 'ios' ? Dimens.dimen_5 : 0,
  },
  locationText: {
    fontFamily: fonts.HelveticaNeueMedium,
    fontSize: Dimens.dimen_16,
    marginTop: Platform.OS === 'ios' ? Dimens.dimen_5 : 0,
    color: Colors.lightblue,
  },
  italicStyle: {
    fontSize: Dimens.dimen_14,
    fontFamily: Fonts.HelveticaNeueItalic,
  },
});

export default styles;
