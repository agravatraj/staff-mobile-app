import {StyleSheet, Platform} from 'react-native';
import {Colors} from '../../../Utils/Colors';
import Fonts from '../../../Utils/Fonts';
import {Dimens} from '../../../Utils/Dimens';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  container2: {
    flexGrow: 1,
    paddingTop: Dimens.dimen_30,
  },
  card1: {
    backgroundColor: Colors.white,
    justifyContent: 'center',
    padding: Dimens.dimen_20,
    elevation: Dimens.dimen_5,
    borderRadius: Dimens.dimen_15,
    shadowColor: Colors.black,
    shadowOffset: {width: 0, height: Dimens.dimen_3},
    shadowOpacity: 0.5,
    shadowRadius: Dimens.dimen_5,
    paddingVertical: Dimens.dimen_20,
    marginHorizontal: Dimens.dimen_15,
    marginVertical: Dimens.dimen_6,
  },
  card1titleStyle: {
    fontSize: Dimens.dimen_14,
    fontFamily: Fonts.HelveticaNeue,
    color: Colors.lightblue,
  },
  cardTitleText: {
    fontSize: Dimens.dimen_14,
    fontFamily: Fonts.helveticaNeueBold,
    color: Colors.lightblue,
    marginBottom: Platform.OS === 'ios' ? Dimens.dimen_10 : Dimens.dimen_5,
  },
  cardContentText: {
    fontSize: Dimens.dimen_11,
    fontFamily: Fonts.HelveticaNeue,
  },
  boldtextStyle: {
    color: Colors.white,
    fontSize: Dimens.dimen_30,
    fontFamily: Fonts.helveticaNeueBold,
  },
});

export default styles;
