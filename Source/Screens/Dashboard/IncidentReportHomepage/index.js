/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Text,
  ScrollView,
  FlatList,
  RefreshControl,
  TouchableOpacity,
  Alert,
} from 'react-native';
import Header from '../../../Components/Header';
import styles from './styles';
import HeaderBig from '../../../Components/HeaderBig';
import {Colors} from '../../../Utils/Colors';
import {Dimens} from '../../../Utils/Dimens';
import {Plus2, Usericon, ListDots} from '../../../Utils/Svg';

const Mydata = [
  {
    id: '1',
    title: 'Customer1',
    name: 'Naser Al zier',
    date: '20/6/2020',
    time: '05:00',
  },
  {
    id: '2',
    title: 'Customer2',
    name: 'Agravat Raj',
    date: '22/4/1997',
    time: '06:00',
  },
];

export default class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <View
          style={{
            flex: 1,
            position: 'absolute',
            left: 0,
            right: 0,
            top: 50,
            bottom: 0,
          }}>
          <ScrollView bounces={false} contentContainerStyle={{flexGrow: 1}}>
            <View style={styles.container}>
              <HeaderBig
                title={
                  <Text>
                    My{' '}
                    <Text style={styles.boldtextStyle}>Incident reports</Text>
                  </Text>
                }
                backButton={false}
                backPress={() => {
                  this.props.navigation.goBack();
                }}
              />
              <View style={styles.container2}>
                <FlatList
                  data={Mydata}
                  renderItem={this.renderItem}
                  keyExtractor={(item) => item.id}
                  showsVerticalScrollIndicator={false}
                  style={{flex: 1}}
                  contentContainerStyle={{flexGrow: 1}}
                  refreshControl={
                    <RefreshControl
                      colors={['#9Bd35A', '#689F38']}
                      refreshing={this.state.refreshing}
                      onRefresh={() => {
                        this.setState({refreshing: false});
                      }}
                    />
                  }
                />
              </View>
            </View>
          </ScrollView>
        </View>
        <Header onPress={() => this.props.navigation.openDrawer()} />
        <View style={styles.messageButtonViewStyle} pointerEvents="box-none">
          <TouchableOpacity
            style={styles.messageButtonStyle}
            onPress={() => {
              this.props.navigation.navigate('NewIncidentReportScreen');
            }}>
            <Plus2 />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
  renderItem = ({item}) => (
    <TouchableOpacity style={styles.card1} onPress={() => {}}>
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        <Text style={styles.cardTitleText}>{item.title}</Text>
        <TouchableOpacity
          style={{paddingHorizontal: 10}}
          onPress={() => {
            this.props.navigation.navigate('IncidentReportDetailScreen');
          }}>
          <ListDots />
        </TouchableOpacity>
      </View>
      <Text style={styles.dateTextStyle}>{item.name}</Text>
      <Text style={styles.dateTextStyle}>
        Incident date{' '}
        <Text style={styles.dateStyle}>
          {item.date} <Text>{item.time}</Text>
        </Text>{' '}
      </Text>
    </TouchableOpacity>
  );
}
