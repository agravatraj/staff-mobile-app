import {StyleSheet, Platform} from 'react-native';
import {Colors} from '../../../Utils/Colors';
import Fonts from '../../../Utils/Fonts';
import {Dimens} from '../../../Utils/Dimens';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  container2: {
    flexGrow: 1,
    paddingTop: Dimens.dimen_30,
  },
  card1: {
    backgroundColor: Colors.white,
    padding: Dimens.dimen_20,
    elevation: Dimens.dimen_5,
    borderRadius: Dimens.dimen_15,
    shadowColor: Colors.black,
    shadowOffset: {width: 0, height: Dimens.dimen_3},
    shadowOpacity: 0.5,
    shadowRadius: Dimens.dimen_5,
    paddingVertical: Dimens.dimen_15,
    marginHorizontal: Dimens.dimen_15,
    marginVertical: Dimens.dimen_6,
    flexDirection: 'row',
  },
  card1titleStyle: {
    fontSize: Dimens.dimen_14,
    fontFamily: Fonts.HelveticaNeue,
    color: Colors.lightblue,
  },
  cardTitleText: {
    fontSize: Dimens.dimen_14,
    fontFamily: Fonts.helveticaNeueBold,
    color: Colors.black,
    marginBottom: Platform.OS === 'ios' ? 0 : -5,
  },
  cardContentText: {
    fontSize: Dimens.dimen_14,
    fontFamily: Fonts.HelveticaNeueLight,
  },
  boldtextStyle: {
    color: Colors.white,
    fontSize: Dimens.dimen_30,
    fontFamily: Fonts.helveticaNeueBold,
  },
  dateStyle: {
    fontSize: Dimens.dimen_12,
    fontFamily: Fonts.HelveticaNeueLight,
    color: Colors.dateColor,
  },
  messageButtonViewStyle: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingRight: Dimens.dimen_30,
    paddingBottom: Dimens.dimen_30,
    // backgroundColor:'pink'
  },
  messageButtonStyle: {
    height: Dimens.dimen_60,
    width: Dimens.dimen_60,
    backgroundColor: Colors.orange,
    borderRadius: Dimens.dimen_30,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'flex-end',
  },
});

export default styles;
