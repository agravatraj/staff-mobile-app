/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Text,
  ScrollView,
  FlatList,
  RefreshControl,
  TouchableOpacity,
} from 'react-native';
import Header from '../../../Components/Header';
import styles from './styles';
import HeaderBig from '../../../Components/HeaderBig';
import {Colors} from '../../../Utils/Colors';
import {Dimens} from '../../../Utils/Dimens';
import {Usericon, MessageButton} from '../../../Utils/Svg';

const Mydata = [
  {
    id: 1,
    title: 'Raj Agravat',
    content: 'this is test message for raj',
  },
  {
    id: 2,
    title: 'Mitesh baraiya',
    content: 'this is test message for mitesh',
  },
  {
    id: 3,
    title: 'Jaydeep jani',
    content: 'this is test message for Jaydeep',
  },
  {
    id: 4,
    title: 'Vipul Test',
    content: 'this is test message',
  },
  {
    id: 5,
    title: 'Vipul Test',
    content: 'this is test message',
  },
  {
    id: 6,
    title: 'Vipul Test',
    content: 'this is test message',
  },
  {
    id: 7,
    title: 'Vipul Test',
    content: 'this is test message',
  },
  {
    id: 8,
    title: 'Vipul Test',
    content: 'this is test message',
  },
  {
    id: 9,
    title: 'Vipul Test',
    content: 'this is test message',
  },
];

export default class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <View
          style={{
            flex: 1,
            position: 'absolute',
            left: 0,
            right: 0,
            top: 50,
            bottom: 0,
          }}>
          <ScrollView bounces={false} contentContainerStyle={{flexGrow: 1}}>
            <View style={styles.container}>
              <HeaderBig
                title={<Text style={styles.boldtextStyle}>Messages</Text>}
                backButton={true}
                backPress={() => {
                  this.props.navigation.goBack();
                }}
              />
              <View style={styles.container2}>
                <FlatList
                  data={Mydata}
                  renderItem={this.renderItem}
                  keyExtractor={(item) => item.id + ''}
                  showsVerticalScrollIndicator={false}
                  style={{flex: 1}}
                  contentContainerStyle={{flexGrow: 1}}
                  refreshControl={
                    <RefreshControl
                      colors={['#9Bd35A', '#689F38']}
                      refreshing={this.state.refreshing}
                      onRefresh={() => {
                        this.setState({refreshing: false});
                      }}
                    />
                  }
                />
              </View>
            </View>
          </ScrollView>
        </View>
        <Header onPress={() => this.props.navigation.openDrawer()} />
        <View style={styles.messageButtonViewStyle} pointerEvents="box-none">
          <TouchableOpacity
            style={styles.messageButtonStyle}
            onPress={() => {
              this.props.navigation.navigate('NewMessageScreen');
            }}>
            <MessageButton />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
  renderItem = ({item}) => (
    <TouchableOpacity
      style={styles.card1}
      onPress={() => {
        this.props.navigation.navigate('MessageThreadScreen', {
          personName: item.title,
          message: item.content,
        });
      }}>
      <View style={{flex: 1, justifyContent: 'center'}}>
        <Usericon
          fill={Colors.orange}
          height={Dimens.dimen_35}
          width={Dimens.dimen_35}
        />
      </View>
      <View style={{flex: Dimens.dimen_5}}>
        <Text style={styles.cardTitleText}>{item.title}</Text>
        <Text style={styles.cardContentText}>{item.content}</Text>
      </View>
      <View style={{flex: 1, alignItems: 'flex-end'}}>
        <Text style={styles.dateStyle}>14 Dec</Text>
      </View>
    </TouchableOpacity>
  );
}
