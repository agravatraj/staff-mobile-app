/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text, ScrollView, TextInput, Alert} from 'react-native';
import Header from '../../../Components/Header';
import styles from './styles';
import HeaderBig from '../../../Components/HeaderBig';
import {Dimens} from '../../../Utils/Dimens';
import {Plus, Send, Photo, Camera, DropdownArrow} from '../../../Utils/Svg';
import CustomeButton from '../../../Components/CustomeButton';
import {Colors} from '../../../Utils/Colors';
import Fonts from '../../../Utils/Fonts';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Modal from 'react-native-modal';
import PushPopButton from '../../../Components/PushPopButton';
import {AccordionList} from 'accordion-collapse-react-native';
import CustomeRadioButton from '../../../Components/CustomeRadioButton';

export default class index extends React.Component {
  constructor(props) {
    console.log('constructor called');
    super(props);
    this.state = {
      refreshing: false,
      modalVisible: false,
      modal2Visible: false,
      modal3Visible: false,
      venueName: '',
      reportedBy: '',
      dateTimeOfInc: '',
      LocOfIncOther: '',
      seriousOther: '',
      refuseEntryReason: '',
      additionalIncDetail: '',
      myDateAndTime: '',
      list: [
        {
          id: 1,
          title: 'Person of interest 1',
          name: 'Agravat Raj',
        },
      ],
    };
    this.setNewDateAndTime();
  }
  setNewDateAndTime = () => {
    console.log('setNewDateAndTime called');
    var d = new Date();
    var myDate =
      d.getDate() +
      '/' +
      (d.getMonth() + 1) +
      '/' +
      d.getFullYear() +
      '  ' +
      d.getHours() +
      ':' +
      d.getMinutes();

    this.setState({myDateAndTime: myDate});
  };
  _head(item) {
    return (
      <View style={styles.headerView}>
        <Text style={styles.headerText}>{item.title}</Text>
      </View>
    );
  }

  _body = (item) => {
    return (
      <View style={{padding: 10}}>
        <View style={styles.card1}>
          <View style={{flex: 1}}>
            <Text style={styles.labelStyle}>Name</Text>
          </View>
          <View style={{flex: 1}}>
            <TextInput
              onChangeText={(txt) => {
                this.setState({name: txt});
              }}
              style={styles.TextInputTextStyle}
            />
          </View>
        </View>
        <View style={styles.card1}>
          <View style={{flex: 1}}>
            <Text style={styles.labelStyle}>Phone number</Text>
          </View>
          <View style={{flex: 1}}>
            <TextInput
              onChangeText={(txt) => {
                this.setState({phoneNumber: txt});
              }}
              style={styles.TextInputTextStyle}
            />
          </View>
        </View>
        <View style={styles.card1}>
          <Text style={styles.labelStyle}>Age</Text>
          <TouchableOpacity
            onPress={() => {
              this.setState({modal1Visible: true});
            }}>
            <DropdownArrow />
          </TouchableOpacity>
          <Modal
            isVisible={this.state.modal1Visible}
            backdropOpacity={0}
            style={{
              justifyContent: 'flex-end',
              paddingBottom: 60,
              paddingHorizontal: 30,
            }}
            onBackdropPress={() => {
              this.setState({
                modal1Visible: false,
              });
            }}>
            <View style={styles.modalView}>
              <View style={styles.modalView1_1}>
                <PushPopButton title="Under 18" />
                <PushPopButton title="18-25" />
                <PushPopButton title="26-34" />
                <PushPopButton title="35+" />
              </View>
              <View style={styles.cancelViewStyle}>
                <Text
                  style={styles.cancel}
                  onPress={() => {
                    this.setState({modal1Visible: false});
                  }}>
                  Cancel
                </Text>
                <Text style={styles.add}>Add</Text>
              </View>
            </View>
          </Modal>
        </View>
        <View style={styles.card1}>
          <Text style={styles.labelStyle}>Gender</Text>
          <TouchableOpacity
            onPress={() => {
              this.setState({modal2Visible: true});
            }}>
            <DropdownArrow />
          </TouchableOpacity>
          <Modal
            isVisible={this.state.modal2Visible}
            backdropOpacity={0}
            style={{
              justifyContent: 'flex-end',
              paddingBottom: 60,
              paddingHorizontal: 30,
            }}
            onBackdropPress={() => {
              this.setState({
                modal2Visible: false,
              });
            }}>
            <View style={styles.modalView}>
              <View style={styles.modalView1_1}>
                <PushPopButton title="Male" />
                <PushPopButton title="Female" />
                <PushPopButton title="Other" />
              </View>
              <View style={styles.cancelViewStyle}>
                <Text
                  style={styles.cancel}
                  onPress={() => {
                    this.setState({modal2Visible: false});
                  }}>
                  Cancel
                </Text>
                <Text style={styles.add}>Add</Text>
              </View>
            </View>
          </Modal>
        </View>
        <View
          style={[
            styles.card1,
            {flexDirection: 'column', alignItems: 'flex-start', height: null},
          ]}>
          <Text style={styles.labelStyle}>Person(s) Description</Text>
          <View style={{paddingLeft: 30, paddingTop: 10}}>
            <TextInput
              placeholder=""
              multiline={true}
              style={styles.TextInputTextStyle}
              onChangeText={(txt) => {
                this.setState({Persons2Description: txt});
              }}
            />
          </View>
        </View>
      </View>
    );
  };
  render() {
    console.log('render called');
    return (
      <View style={styles.container}>
        <Header onPress={() => this.props.navigation.openDrawer()} />
        <View style={{flex: 1}}>
          <ScrollView
            bounces={false}
            contentContainerStyle={{flexGrow: 1}}
            keyboardShouldPersistTaps={'always'}>
            <View style={styles.container}>
              <HeaderBig
                title={
                  <Text>
                    New{' '}
                    <Text style={styles.boldtextStyle}>Incident reports</Text>
                  </Text>
                }
                backButton={false}
                backPress={() => {
                  this.props.navigation.goBack();
                }}
                cancelButton={true}
                cancelPress={() => {
                  this.props.navigation.goBack();
                }}
              />
              <View style={styles.container2}>
                <View style={styles.view1Style}>
                  <View style={styles.view1_1Style}>
                    <View style={styles.view1_2Style}>
                      <Text style={styles.heading1Style}>License Number</Text>
                      <Text style={styles.detail1Style}>0002343234</Text>
                    </View>
                    <View style={styles.view1_3Style}>
                      <Text style={styles.heading1Style}>Incident Number</Text>
                      <Text style={styles.detail1Style}>0002343234</Text>
                    </View>
                  </View>
                  <View style={{flexDirection: 'row', flex: 1}}>
                    <View style={styles.view1_2Style}>
                      <Text style={styles.heading1Style}>
                        Entry date & time
                      </Text>
                      <Text style={styles.detail1Style}>
                        {this.state.myDateAndTime}
                      </Text>
                    </View>
                    <View style={styles.view1_3Style}>
                      <Text style={styles.heading1Style}>Entered by</Text>
                      <Text style={styles.detail1Style}>Naser Al Zier</Text>
                    </View>
                  </View>
                </View>
                <View style={styles.card1}>
                  <View style={{flex: 1}}>
                    <Text style={styles.labelStyle}>Venue name</Text>
                  </View>
                  <View style={{flex: 1}}>
                    <TextInput
                      onChangeText={(txt) => {
                        this.setState({venueName: txt});
                      }}
                      // value={'The club house'}
                      style={styles.TextInputTextStyle}
                    />
                  </View>
                </View>
                <View style={styles.card1}>
                  <View style={{flex: 1}}>
                    <Text style={styles.labelStyle}>Reported By</Text>
                  </View>
                  <View style={{flex: 1}}>
                    <TextInput
                      onChangeText={(txt) => {
                        this.setState({reportedBy: txt});
                      }}
                      // value={'VJames miller'}
                      style={styles.TextInputTextStyle}
                    />
                  </View>
                </View>
                <View style={styles.card1}>
                  <View style={{flex: 1}}>
                    <Text style={styles.labelStyle}>Date/time of incident</Text>
                  </View>
                  <View style={{flex: 1}}>
                    <TextInput
                      onChangeText={(txt) => {
                        this.setState({dateTimeOfInc: txt});
                      }}
                      // value={'14/01/2020 21:30'}
                      style={styles.TextInputTextStyle}
                    />
                  </View>
                </View>
                <View style={styles.card1}>
                  <Text style={styles.labelStyle}>Location of incident</Text>
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({modalVisible: true});
                    }}>
                    <DropdownArrow />
                  </TouchableOpacity>
                  <Modal
                    backdropOpacity={0}
                    style={{
                      justifyContent: 'flex-end',
                      paddingBottom: 20,
                      paddingHorizontal: 30,
                    }}
                    isVisible={this.state.modalVisible}
                    onBackdropPress={() => {
                      this.setState({modalVisible: false});
                    }}>
                    <View style={styles.modalView}>
                      <View style={styles.modalView1_1}>
                        <PushPopButton title="Refuse entry" />
                        <PushPopButton title="Refuse service" />
                        <PushPopButton title="Theft" />
                        <PushPopButton title="Malicious damage" />
                        <PushPopButton title="Complaint" />
                        <PushPopButton title="Minors" />
                        <PushPopButton title="Self exclusion" />
                        <PushPopButton title="Gaming" />
                      </View>
                      <View style={styles.cancelViewStyle}>
                        <Text
                          style={styles.cancel}
                          onPress={() => {
                            this.setState({modalVisible: false});
                          }}>
                          Cancel
                        </Text>
                        <Text style={styles.add}>Add</Text>
                      </View>
                    </View>
                  </Modal>
                </View>
                <View style={styles.card1}>
                  <View style={{flex: 1}}>
                    <Text style={styles.labelStyle}>
                      Location of incident (others)
                    </Text>
                  </View>
                  <View style={{flex: 1}}>
                    <TextInput
                      onChangeText={(txt) => {
                        this.setState({LocOfIncOther: txt});
                      }}
                      style={styles.TextInputTextStyle}
                    />
                  </View>
                </View>
                <View style={styles.card1}>
                  <Text style={styles.labelStyle}>Incident details</Text>
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({modal2Visible: true});
                    }}>
                    <DropdownArrow />
                  </TouchableOpacity>
                  <Modal
                    backdropOpacity={0}
                    style={{
                      justifyContent: 'flex-end',
                      paddingBottom: 20,
                      paddingHorizontal: 30,
                    }}
                    isVisible={this.state.modal2Visible}
                    onBackdropPress={() => {
                      this.setState({
                        modalVisible: false,
                        modal2Visible: false,
                      });
                    }}>
                    <View style={styles.modalView}>
                      <View style={styles.modalView1_1}>
                        <PushPopButton title="Insufficient ID" />
                        <PushPopButton title="Minors" />
                        <PushPopButton title="Suspected intoxication" />
                      </View>
                      <View style={styles.cancelViewStyle}>
                        <Text
                          style={styles.cancel}
                          onPress={() => {
                            this.setState({modal2Visible: false});
                          }}>
                          Cancel
                        </Text>
                        <Text style={styles.add}>Add</Text>
                      </View>
                    </View>
                  </Modal>
                </View>
                <View style={styles.card1}>
                  <View style={{flex: 1}}>
                    <Text style={styles.labelStyle}>Serious (other)</Text>
                  </View>
                  <View style={{flex: 1}}>
                    <TextInput
                      onChangeText={(txt) => {
                        this.setState({seriousOther: txt});
                      }}
                      style={styles.TextInputTextStyle}
                    />
                  </View>
                </View>
                <View style={styles.card1}>
                  <View style={{flex: 1}}>
                    <Text style={styles.labelStyle}>Refuse entry reason</Text>
                  </View>
                  <View style={{flex: 1}}>
                    <TextInput
                      onChangeText={(txt) => {
                        this.setState({refuseEntryReason: txt});
                      }}
                      style={styles.TextInputTextStyle}
                    />
                  </View>
                </View>
                <View style={styles.card1}>
                  <View style={{flex: 1}}>
                    <Text style={styles.labelStyle}>
                      Additional incident details
                    </Text>
                  </View>
                  <View style={{flex: 1}}>
                    <TextInput
                      onChangeText={(txt) => {
                        this.setState({additionalIncDetail: txt});
                      }}
                      style={styles.TextInputTextStyle}
                    />
                  </View>
                </View>
                <View style={styles.card1}>
                  <Text style={styles.labelStyle}>Asked to leave reason</Text>
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({modal3Visible: true});
                    }}>
                    <DropdownArrow />
                  </TouchableOpacity>
                  <Modal
                    backdropOpacity={0}
                    style={{
                      justifyContent: 'flex-end',
                      paddingBottom: 20,
                      paddingHorizontal: 30,
                    }}
                    isVisible={this.state.modal3Visible}
                    onBackdropPress={() => {
                      this.setState({modal3Visible: false});
                    }}>
                    <View style={styles.modalView}>
                      <View style={styles.modalView1_1}>
                        <PushPopButton title="Approaching intoxication" />
                        <PushPopButton title="Suspected intoxication" />
                        <PushPopButton title="Violent" />
                        <PushPopButton title="Anti-social" />
                        <PushPopButton title="Illicit substances" />
                        <PushPopButton title="Insufficient ID" />
                        <PushPopButton title="Minor" />
                        <PushPopButton title="Smoking in no-smoking area" />
                      </View>
                      <View style={styles.cancelViewStyle}>
                        <Text
                          style={styles.cancel}
                          onPress={() => {
                            this.setState({modal3Visible: false});
                          }}>
                          Cancel
                        </Text>
                        <Text style={styles.add}>Add</Text>
                      </View>
                    </View>
                  </Modal>
                </View>

                <View
                  style={[
                    styles.card1,
                    {
                      padding: 0,
                      paddingVertical: 0,
                      height: 40,
                      paddingLeft: 0,
                    },
                  ]}>
                  <View style={styles.nofpiView}>
                    <Text style={styles.labelStyle}>
                      No of Persons Involved
                    </Text>
                  </View>
                  <View style={styles.nofpiView2}>
                    <Text>3</Text>
                  </View>
                </View>
                <View
                  style={[
                    styles.card1,
                    {
                      flexDirection: 'column',
                      alignItems: 'flex-start',
                      height: null,
                    },
                  ]}>
                  <Text style={styles.labelStyle}>Person(s) Description</Text>
                  <View style={{paddingLeft: 30, paddingTop: 10}}>
                    <TextInput
                      placeholder=""
                      multiline={true}
                      style={styles.TextInputTextStyle}
                      onChangeText={(txt) => {
                        this.setState({persons1Description: txt});
                      }}
                    />
                  </View>
                </View>
                <View>
                  <AccordionList
                    list={this.state.list}
                    header={this._head}
                    body={this._body}
                    keyExtractor={(item) => `${item.id}`}
                  />
                </View>
                <View style={styles.card1}>
                  <Text style={styles.labelStyle}>Action taken/ details</Text>
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({modalVisible: true});
                    }}>
                    <DropdownArrow />
                  </TouchableOpacity>
                  <Modal
                    backdropOpacity={0}
                    style={{
                      justifyContent: 'center',
                      paddingBottom: 20,
                      paddingHorizontal: 30,
                    }}
                    isVisible={this.state.modalVisible}
                    onBackdropPress={() => {
                      this.setState({modalVisible: false});
                    }}>
                    <View style={styles.modalView}>
                      <View style={styles.modalView1_1}>
                        <PushPopButton title="Patron refused entry" />
                        <PushPopButton title="Patron refused service" />
                        <PushPopButton title="Patron asked to leave" />
                        <PushPopButton title="First aid treatment supplied" />
                        <PushPopButton title="Ambulance attended" />
                        <PushPopButton title="Security attended" />
                        <PushPopButton title="Police called by venue staff" />
                        <PushPopButton title="Police involved" />
                      </View>
                      <View style={styles.cancelViewStyle}>
                        <Text
                          style={styles.cancel}
                          onPress={() => {
                            this.setState({modalVisible: false});
                          }}>
                          Cancel
                        </Text>
                        <Text style={styles.add}>Add</Text>
                      </View>
                    </View>
                  </Modal>
                </View>
                <View style={[styles.card1, {flexDirection: 'column'}]}>
                  <Text style={styles.labelStyle}>Incident summary report</Text>
                  <View style={{paddingLeft: 30, paddingTop: 10}}>
                    <TextInput
                      placeholder=""
                      multiline={true}
                      style={styles.TextInputTextStyle}
                      onChangeText={(txt) => {
                        this.setState({incidentSummaryReport: txt});
                      }}
                    />
                  </View>
                </View>
                <View style={styles.card1}>
                  <View style={styles.leftView}>
                    <Text style={styles.labelStyle}>
                      No of Persons Involved
                    </Text>
                  </View>
                  <View style={styles.rightView}>
                    <CustomeRadioButton />
                  </View>
                </View>
                <View style={styles.card1}>
                  <View style={styles.leftView}>
                    <Text style={styles.labelStyle}>Captured on CCTV?</Text>
                  </View>
                  <View style={styles.rightView}>
                    <CustomeRadioButton />
                  </View>
                </View>
                <View style={styles.card1}>
                  <View style={styles.leftView}>
                    <Text style={styles.labelStyle}>CCTV burned to disc?</Text>
                  </View>
                  <View style={styles.rightView}>
                    <CustomeRadioButton />
                  </View>
                </View>
                <View style={styles.card1}>
                  <View style={styles.leftView}>
                    <Text style={styles.labelStyle}>
                      Linked to other incidents
                    </Text>
                  </View>
                  <View style={styles.rightView}>
                    <CustomeRadioButton />
                  </View>
                </View>
                <View style={styles.card1}>
                  <View style={{flex: 1}}>
                    <Text style={styles.labelStyle}>Location of incident</Text>
                  </View>
                  <View style={{flex: 1}}>
                    <TextInput
                      onChangeText={(txt) => {
                        this.setState({locationOfIncident: txt});
                      }}
                      style={styles.TextInputTextStyle}
                    />
                  </View>
                </View>
                <View style={styles.card1}>
                  <View style={{flex: 1}}>
                    <Text style={styles.labelStyle}>Incident details</Text>
                  </View>
                  <View style={{flex: 1}}>
                    <TextInput
                      onChangeText={(txt) => {
                        this.setState({incidentDetail: txt});
                      }}
                      style={styles.TextInputTextStyle}
                    />
                  </View>
                </View>
                <View style={styles.card1}>
                  <View style={{flex: 1}}>
                    <Text style={styles.labelStyle}>Manager comments</Text>
                  </View>
                  <View style={{flex: 1}}>
                    <TextInput
                      onChangeText={(txt) => {
                        this.setState({managerComment: txt});
                      }}
                      style={styles.TextInputTextStyle}
                    />
                  </View>
                </View>
                <View style={styles.card1}>
                  <View style={styles.leftView}>
                    <Text style={styles.labelStyle}>Manager approved?</Text>
                  </View>
                  <View style={styles.rightView}>
                    <CustomeRadioButton />
                  </View>
                </View>
                <View style={styles.card1}>
                  <View style={{flex: 1}}>
                    <Text style={styles.labelStyle}>Manager details</Text>
                  </View>
                  <View style={{flex: 1}}>
                    <TextInput
                      onChangeText={(txt) => {
                        this.setState({managerDetail: txt});
                      }}
                      style={styles.TextInputTextStyle}
                    />
                  </View>
                </View>
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
}
