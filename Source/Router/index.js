import React from 'react';

import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import {createDrawerNavigator} from '@react-navigation/drawer';

import SplashScreen from '../../Source/Screens/Authentications/Splash';
import LoginScreen from '../../Source/Screens/Authentications/Login';
import ResetPasswordScreen from '../../Source/Screens/Authentications/ResetPassword';
import CustomerDbScreen from '../../Source/Screens/Dashboard/CustomerDashboard';
import DrawerContent from '../../Source/Components/DrawerContent';
import SupportScreen from '../../Source/Screens/Dashboard/Support';
import PrivacyScreen from '../../Source/Screens/Dashboard/Privacy';
import UserAgreementScreen from '../../Source/Screens/Dashboard/UserAgreement';
import MessageScreen from '../../Source/Screens/Dashboard/Messages';
import MessageThreadScreen from '../../Source/Screens/Dashboard/Message_thread';
import NewMessageScreen from '../../Source/Screens/Dashboard/NewMessage';
import MyProfileScreen from '../../Source/Screens/Dashboard/MyProfile';
import MyScheduleScreen from '../../Source/Screens/Dashboard/MySchedule';
import ShiftDetailScreen from '../../Source/Screens/Dashboard/ShiftDetail';
import AvailabiltyRequestScreen from '../../Source/Screens/Dashboard/AvailabilityRequest';
import AvailabiltyRequestNewRequestScreen from '../../Source/Screens/Dashboard/AvailabilityRequestNewRequest';
import PastShiftsScreen from '../../Source/Screens/Dashboard/PastShifts';
import IncidentReportHomepageScreen from '../../Source/Screens/Dashboard/IncidentReportHomepage';
import NewIncidentReportScreen from '../../Source/Screens/Dashboard/NewIncidentReport';
import NewIncidentReportPage2Screen from '../../Source/Screens/Dashboard/NewIncidentReportPage2';
import NewIncidentReportPage3Screen from '../../Source/Screens/Dashboard/NewIncidentReportPage3';
import IncidentReportDetailScreen from '../../Source/Screens/Dashboard/IncidentReportDetails';
import FBLoginScreen from '../../Source/Screens/Dashboard/FacebookLogin'
const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

function MyDrawer() {
  return (
    <Drawer.Navigator drawerContent={(props) => <DrawerContent {...props} />}>
      <Drawer.Screen name="CustomerDbScreen" component={CustomerDbScreen} />
      <Drawer.Screen name="SupportScreen" component={SupportScreen} />
      <Drawer.Screen name="PrivacyScreen" component={PrivacyScreen} />
      <Drawer.Screen
        name="UserAgreementScreen"
        component={UserAgreementScreen}
      />
      <Drawer.Screen name="MessageScreen" component={MessageScreen} />
      <Drawer.Screen
        name="MessageThreadScreen"
        component={MessageThreadScreen}
      />
      <Drawer.Screen name="NewMessageScreen" component={NewMessageScreen} />
      <Drawer.Screen name="MyProfileScreen" component={MyProfileScreen} />
      <Drawer.Screen name="MyScheduleScreen" component={MyScheduleScreen} />
      <Drawer.Screen name="ShiftDetailScreen" component={ShiftDetailScreen} />
      <Drawer.Screen
        name="AvailabiltyRequestScreen"
        component={AvailabiltyRequestScreen}
      />
      <Drawer.Screen
        name="AvailabiltyRequestNewRequestScreen"
        component={AvailabiltyRequestNewRequestScreen}
      />
      <Drawer.Screen name="PastShiftsScreen" component={PastShiftsScreen} />
      <Drawer.Screen
        name="IncidentReportHomepageScreen"
        component={IncidentReportHomepageScreen}
      />
      <Drawer.Screen
        name="NewIncidentReportScreen"
        component={NewIncidentReportScreen}
      />
      <Drawer.Screen
        name="NewIncidentReportPage2Screen"
        component={NewIncidentReportPage2Screen}
      />
      <Drawer.Screen
        name="NewIncidentReportPage3Screen"
        component={NewIncidentReportPage3Screen}
      />
      <Drawer.Screen
        name="IncidentReportDetailScreen"
        component={IncidentReportDetailScreen}
      />
    </Drawer.Navigator>
  );
}

function Router() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="FBLoginScreen" headerMode="none">
        <Stack.Screen name="SplashScreen" component={SplashScreen} />
        <Stack.Screen name="LoginScreen" component={LoginScreen} />
        <Stack.Screen
          name="ResetPasswordScreen"
          component={ResetPasswordScreen}
        />
        <Stack.Screen name="CustomerDbScreen" component={MyDrawer} />
        <Stack.Screen name="FBLoginScreen" component={FBLoginScreen} />
     
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Router;
