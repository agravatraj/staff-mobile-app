const Fonts = {
  openSansRegular: 'OpenSans-Regular',
  openSansLight: 'OpenSans-Light',
  openSansBold: 'OpenSans-Bold',
  helveticaNeueBold: 'HelveticaNeue-Bold',
  HelveticaNeueLight: 'HelveticaNeue-Light',
  HelveticaNeueMedium: 'HelveticaNeue-Medium',
  HelveticaNeueThin: 'HelveticaNeue-Thin',
  HelveticaNeueUltraLight: 'HelveticaNeue-UltraLight',
  HelveticaNeue: 'HelveticaNeue',
  SourceSansProBold: 'SourceSansPro-Bold',
  SourceSansProRegular: 'SourceSansPro-Regular',
  HelveticaNeueItalic: 'HelveticaNeue-Italic',
};

export default Fonts;
