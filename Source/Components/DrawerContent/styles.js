import {StyleSheet} from 'react-native';
import {Colors} from '../../Utils/Colors';
import fonts from '../../Utils/Fonts';
import {Dimens} from '../../Utils/Dimens';

const styles = StyleSheet.create({
  container: {
    height: Dimens.dimen_150,
    backgroundColor: Colors.lightblue,
    alignItems: 'center',
    justifyContent: 'center',
  },
  nameStyle: {
    fontSize: Dimens.dimen_20,
    fontFamily: fonts.HelveticaNeue,
    color: Colors.white,
  },
  numberStyle: {
    fontSize: Dimens.dimen_16,
    fontFamily: fonts.HelveticaNeue,
    color: Colors.white,
  },
});

export default styles;
