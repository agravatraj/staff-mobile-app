import React from 'react';
import {View, Text, ScrollView} from 'react-native';
import styles from './styles';
import {Usericon} from '../../Utils/Svg';
import MyDrawerButton from '../../Components/CustomeButtonForDrawer';

export default class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View>
        <View style={styles.container}>
          <Usericon />
          <Text style={styles.nameStyle}>Naser Al Zier</Text>
          <Text style={styles.numberStyle}>000042918</Text>
        </View>
        <ScrollView>
          <MyDrawerButton title="Home" icon="home" onPress={() => {}} />
          <MyDrawerButton
            title="My schedule"
            icon="mySchedule"
            onPress={() => {
              this.props.navigation.closeDrawer();
              this.props.navigation.navigate('MyScheduleScreen');
            }}
          />
          <MyDrawerButton
            title="Past shifts"
            icon="pastShifts"
            onPress={() => {
              this.props.navigation.closeDrawer();
              this.props.navigation.navigate('PastShiftsScreen');
            }}
          />
          <MyDrawerButton
            title="Availability Request"
            icon="request"
            onPress={() => {
              this.props.navigation.closeDrawer();
              this.props.navigation.navigate('AvailabiltyRequestScreen');
            }}
          />
          <MyDrawerButton
            title="My Profile"
            icon="myprofile"
            onPress={() => {
              this.props.navigation.closeDrawer();
              this.props.navigation.navigate('MyProfileScreen');
            }}
          />
          <MyDrawerButton
            title="Incident Reports"
            icon="repoters"
            onPress={() => {
              this.props.navigation.closeDrawer();
              this.props.navigation.navigate('IncidentReportHomepageScreen');
            }}
          />
          <MyDrawerButton
            title="Messages"
            icon="message"
            onPress={() => {
              this.props.navigation.closeDrawer();
              this.props.navigation.navigate('MessageScreen');
            }}
          />
          <MyDrawerButton
            title="Help"
            icon="questionmark"
            onPress={() => {
              this.props.navigation.closeDrawer();
              this.props.navigation.navigate('SupportScreen');
            }}
          />
          <MyDrawerButton title="Logout" icon="logout" onPress={() => {}} />
        </ScrollView>
      </View>
    );
  }
}
