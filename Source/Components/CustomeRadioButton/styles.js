import {StyleSheet} from 'react-native';
import {Colors} from '../../Utils/Colors';
import fonts from '../../Utils/Fonts';
import {Dimens} from '../../Utils/Dimens';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  container2: {flex: 1, flexDirection: 'row', alignItems: 'center'},
  RadioStyle: {
    width: Dimens.dimen_20,
    height: Dimens.dimen_20,
    backgroundColor: Colors.white,
    borderRadius: Dimens.dimen_10,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: Dimens.dimen_2,
    marginRight: Dimens.dimen_10,
    borderColor: Colors.lightblue,
  },
  RadioViewStyle: {
    height: Dimens.dimen_10,
    width: Dimens.dimen_10,
    borderRadius: Dimens.dimen_5,
  },
  YesNoStyle: {
    fontSize: Dimens.dimen_13,
    fontFamily: fonts.helveticaNeueBold,
    color: Colors.black,
  },
});

export default styles;
