/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import styles from './styles';
import {Colors} from '../../Utils/Colors';
import Fonts from '../../Utils/Fonts';

export default class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {RadioValue: ''};
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.container2}>
          <TouchableOpacity
            style={styles.RadioStyle}
            onPress={() => {
              this.setState({RadioValue: 'm'});
            }}>
            <View
              style={[
                styles.RadioViewStyle,
                {
                  backgroundColor:
                    this.state.RadioValue === 'm'
                      ? Colors.lightblue
                      : Colors.white,
                },
              ]}
            />
          </TouchableOpacity>

          <Text style={styles.YesNoStyle}>Yes</Text>
        </View>

        <View style={styles.container2}>
          <TouchableOpacity
            style={styles.RadioStyle}
            onPress={() => {
              this.setState({RadioValue: 'f'});
            }}>
            <View
              style={[
                styles.RadioViewStyle,
                {
                  backgroundColor:
                    this.state.RadioValue === 'f'
                      ? Colors.lightblue
                      : Colors.white,
                },
              ]}
            />
          </TouchableOpacity>

          <Text style={styles.YesNoStyle}>No</Text>
        </View>
      </View>
    );
  }
}
