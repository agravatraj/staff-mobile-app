import {StyleSheet} from 'react-native';
import {Colors} from '../../Utils/Colors';
import {Dimens} from '../../Utils/Dimens';

const styles = StyleSheet.create({
  container: {
    marginVertical: Dimens.dimen_5,
    shadowColor: Colors.black,
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowRadius: 4,
    shadowOpacity: 0.6,
  },
  container2: {
    flex: 1,
  },
});

export default styles;
