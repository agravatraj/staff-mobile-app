/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import styles from './styles';

const CustomeButton = (props) => {
  return (
    <View style={props.shadow ? styles.container : styles.container2}>
      <TouchableOpacity
        style={{
          backgroundColor: props.backgroundColor,
          width: props.width,
          height: props.height,
          alignItems: props.alignItems,
          justifyContent: 'center',
          borderRadius: props.borderRadius,
          borderTopLeftRadius: props.borderTopLeftRadius,
          elevation: props.elevation,
          borderWidth: props.borderWidth,
          borderColor: props.borderColor,
        }}
        onPress={props.onPress}>
        <Text
          style={{
            color: props.color,
            fontSize: props.fontSize,
            fontFamily: props.fontFamily,
            textAlign: 'center',
          }}>
          {props.title}
        </Text>
      </TouchableOpacity>
    </View>
  );
};
export default CustomeButton;
