import React from 'react';
import {View, TouchableOpacity} from 'react-native';
import styles from './styles';
import {Menuicon} from '../../Utils/Svg';

const CustomeHeader = (props) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={props.onPress}>
        <Menuicon />
      </TouchableOpacity>
    </View>
  );
};

export default CustomeHeader;
