import {StyleSheet} from 'react-native';
import {Colors} from '../../Utils/Colors';
import {Dimens} from '../../Utils/Dimens';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    height: Dimens.dimen_50,
    backgroundColor: Colors.lightblue,
    alignItems: 'center',
    paddingLeft: Dimens.dimen_20,
  },
});

export default styles;
