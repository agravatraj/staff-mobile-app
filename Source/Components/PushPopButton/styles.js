import {StyleSheet} from 'react-native';
import {Colors} from '../../Utils/Colors';
import {Dimens} from '../../Utils/Dimens';

const styles = StyleSheet.create({
  container: {
    marginVertical: Dimens.dimen_5,
    shadowColor: Colors.black,
    shadowOffset: {
      width: 0,
      height: Dimens.dimen_3,
    },
    shadowRadius: Dimens.dimen_4,
    shadowOpacity: 0.6,
  },
});

export default styles;
