/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import styles from './styles';
import {Colors} from '../../Utils/Colors';
import Fonts from '../../Utils/Fonts';

class CustomeButton extends React.Component {
  static defaultProps = {
    title: 'enter your title',
    backgroundColor: Colors.lightblue,
    justifyContent: 'center',
    borderRadius: 15,
    elevation: 3,
    width: null,
    height: 30,
    alignItems: 'center',
    color: Colors.white,
    fontSize: 14,
    fontFamily: Fonts.HelveticaNeue,
    onPress: () => {},
  };
  constructor(props) {
    super(props);
    this.state = {
      select: false,
    };
  }
  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity
          activeOpacity={0.9}
          style={{
            backgroundColor: this.state.select
              ? this.props.backgroundColor
              : Colors.white,
            width: this.props.width,
            height: this.props.height,
            alignItems: this.props.alignItems,
            justifyContent: this.props.justifyContent,
            borderRadius: this.props.borderRadius,
            elevation: this.props.elevation,
          }}
          onPress={() => {
            this.setState({select: !this.state.select});
          }}>
          <Text
            style={{
              color: this.state.select ? this.props.color : Colors.lightblue,
              fontSize: this.props.fontSize,
              fontFamily: this.props.fontFamily,
              textAlign: 'center',
            }}>
            {this.props.title}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
export default CustomeButton;
