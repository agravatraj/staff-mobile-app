import {StyleSheet} from 'react-native';
import {Colors} from '../../Utils/Colors';
import fonts from '../../Utils/Fonts';
import {Dimens} from '../../Utils/Dimens';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: Dimens.dimen_40,
  },
  touchableOpStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: Dimens.dimen_10,
  },
  txtStyle: {
    color: Colors.black,
    fontSize: Dimens.dimen_14,
    fontFamily: fonts.HelveticaNeue,
  },
  divider: {height: 0.6, backgroundColor: Colors.dividerColor},
});

export default styles;
