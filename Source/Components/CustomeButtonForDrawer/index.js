/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import { Colors } from '../../Utils/Colors';
import styles from './styles';
import fonts from '../../Utils/Fonts'
import {Dimens} from '../../Utils/Dimens'

let Icon = null;

import {Home,MySchedule,AvailabilityRequest,Myprofile,Repoters,Message,Logout,QuestionMark} from '../../Utils/Svg'

class CustomeButton extends React.Component{
  render(){
    if (this.props.icon === 'home') {
      Icon = <Home width={Dimens.dimen_17} height={Dimens.dimen_17}/>;
    } else if (this.props.icon === 'mySchedule') {
      Icon = <MySchedule width={Dimens.dimen_17} height={Dimens.dimen_17}/>;
    } else if (this.props.icon === 'pastShifts') {
      Icon = <MySchedule width={Dimens.dimen_17} height={Dimens.dimen_17}/>;
    }
    else if (this.props.icon === 'request') {
      Icon = <AvailabilityRequest width={Dimens.dimen_17} height={Dimens.dimen_17}/>;
    }
    else if (this.props.icon === 'myprofile') {
      Icon = <Myprofile width={Dimens.dimen_17} height={Dimens.dimen_17}/>;
    }
    else if (this.props.icon === 'repoters') {
      Icon = <Repoters width={Dimens.dimen_17} height={Dimens.dimen_17}/>;
    }
    else if (this.props.icon === 'message') {
      Icon = <Message width={Dimens.dimen_17} height={Dimens.dimen_17}/>;
    }
    else if (this.props.icon === 'questionmark') {
      Icon = <QuestionMark width={Dimens.dimen_17} height={Dimens.dimen_17}/>;
    }
    else if (this.props.icon === 'logout') {
      Icon = <Logout width={Dimens.dimen_17} height={Dimens.dimen_17}/>;
    } else {
      Icon = null;
    }
    return(
      <View style={styles.container}>
      <TouchableOpacity
        style={styles.touchableOpStyle}
        onPress={this.props.onPress}> 
        <View style={{marginHorizontal:30}}>
            {Icon}
        </View>
        <Text
          style={styles.txtStyle}>
          {this.props.title}
        </Text>
      </TouchableOpacity>
      <View style={styles.divider}></View>
    </View>
    )
  }
};
export default CustomeButton;
