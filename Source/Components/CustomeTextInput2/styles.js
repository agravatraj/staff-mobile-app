import {StyleSheet} from 'react-native';
import {Colors} from '../../Utils/Colors';
import fonts from '../../Utils/Fonts';
import {Dimens} from '../../Utils/Dimens';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginVertical: Dimens.dimen_3,
    backgroundColor: Colors.lightblue,
    borderRadius: Dimens.dimen_22,
  },
  container2: {
    flexDirection: 'row',
    marginVertical: Dimens.dimen_3,
    backgroundColor: Colors.greenforBackground,
    borderRadius: Dimens.dimen_22,
  },
  TextInputStyle: {
    paddingLeft: Dimens.dimen_160,
    color: Colors.lightblue,
    fontSize: Dimens.dimen_12,
    fontFamily: fonts.HelveticaNeue,
  },
  TextInputStyle2: {
    paddingLeft: Dimens.dimen_20,
    color: Colors.lightblue,
    fontSize: Dimens.dimen_12,
    fontFamily: fonts.HelveticaNeue,
  },
  nameViewStyle: {
    height: Dimens.dimen_45,
    width: Dimens.dimen_160,
    backgroundColor: Colors.white,
    elevation: Dimens.dimen_2,
    justifyContent: 'center',
    paddingLeft: Dimens.dimen_25,
    borderRadius: Dimens.dimen_25,
    position: 'absolute',
    shadowColor: Colors.black,
    shadowOffset: {width: 0, height: Dimens.dimen_3},
    shadowOpacity: 0.3,
    shadowRadius: Dimens.dimen_3,
  },
  inputViewStyle: {
    flex: 1,
    // height: Dimens.dimen_55,
    justifyContent: 'center',
    paddingHorizontal: Dimens.dimen_20,
    borderRadius: Dimens.dimen_20,
    backgroundColor: Colors.semiWhite2,
  },
  nameStyle: {
    color: Colors.lightblue,
    fontSize: Dimens.dimen_14,
    fontFamily: fonts.openSansBold,
  },
});

export default styles;
