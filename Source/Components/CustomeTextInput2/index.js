/* eslint-disable react/no-string-refs */
import React from 'react';
import {View, Text, TextInput} from 'react-native';
import styles from './styles';
import {Colors} from '../../Utils/Colors';
import {Dimens} from '../../Utils/Dimens';
import fonts from '../../Utils/Fonts';

class CustomeTextInput extends React.Component {
  static defaultProps = {
    title: '',
    secureTextEntry: false,
    editable: true,
    placeholder: '',
    value: '',
    returnKeyType: 'next',
    blurOnSubmit: true,
    textSize: 'regular', //regular, medium
    fontSize: Dimens.dimen_12,
    editTextFontSize: Dimens.dimen_12,
    placeholderTextColor: Colors.lightblue,
    numberOfLines: 1,
    multiline: false,
    height: 48,
    address: false,
    editClick: false,
  };
  focus() {
    this.refs.textInput.focus();
  }
  render() {
    return (
      <View style={this.props.editClick ? styles.container2 : styles.container}>
        <View style={[styles.inputViewStyle, {height: this.props.height}]}>
          <TextInput
            ref={'textInput'}
            // style={
            //   this.props.address
            //     ? styles.TextInputStyle2
            //     : styles.TextInputStyle
            // }
            style={{
              paddingLeft: this.props.address
                ? Dimens.dimen_20
                : Dimens.dimen_160,
              color: this.props.editClick
                ? Colors.greenForText
                : Colors.lightblue,
              fontSize: Dimens.dimen_12,
              fontFamily: fonts.HelveticaNeue,
            }}
            placeholder={this.props.placeholder}
            placeholderTextColor={this.props.placeholderTextColor}
            autoCapitalize="none"
            numberOfLines={this.props.numberOfLines}
            maxLength={this.props.maxLength}
            multiline={this.props.multiline}
            spellCheck={false}
            autoCorrect={false}
            editable={this.props.editable}
            value={this.props.value}
            secureTextEntry={this.props.secureTextEntry}
            keyboardType={this.props.keyboardType}
            returnKeyType={this.props.returnKeyType}
            blurOnSubmit={this.props.blurOnSubmit}
            onSubmitEditing={this.props.onSubmitEditing}
            onChangeText={this.props.onChangeText}
          />
        </View>
        <View style={styles.nameViewStyle}>
          <Text style={styles.nameStyle}>{this.props.title}</Text>
        </View>
      </View>
    );
  }
}
export default CustomeTextInput;
