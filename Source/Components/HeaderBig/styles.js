import {StyleSheet} from 'react-native';
import {Colors} from '../../Utils/Colors';
import fonts from '../../Utils/Fonts';
import {Dimens} from '../../Utils/Dimens';
import {color} from 'react-native-reanimated';

const styles = StyleSheet.create({
  container: {
    height: Dimens.dimen_200,
    backgroundColor: Colors.lightblue,
    paddingTop: Dimens.dimen_5,
    paddingBottom: Dimens.dimen_20,
  },
  container2Style: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
    paddingHorizontal: Dimens.dimen_20,
  },
  text1Style: {
    fontSize: Dimens.dimen_29,
    color: Colors.light,
    fontFamily: fonts.HelveticaNeueMedium,
  },
  text2Style: {
    fontSize: Dimens.dimen_29,
    color: Colors.white,
    fontFamily: fonts.helveticaNeueBold,
  },
  text3Style: {
    fontSize: Dimens.dimen_20,
    color: Colors.white,
    fontFamily: fonts.HelveticaNeueThin,
  },
  backButtonStyle: {
    fontSize: Dimens.dimen_22,
    fontFamily: fonts.helveticaNeueBold,
    color: Colors.white,
  },
  boldtextStyle: {
    color: Colors.white,
    fontSize: Dimens.dimen_30,
    fontFamily: fonts.helveticaNeueBold,
  },
  myStyle1: {
    alignItems: 'center',
  },
  myStyle2: {
    paddingHorizontal: Dimens.dimen_20,
  },
});

export default styles;
