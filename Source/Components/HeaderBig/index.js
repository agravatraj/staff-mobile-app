/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text, TouchableOpacity, Alert} from 'react-native';
import styles from './styles';
import {Building, Usericon, Edit, List, ListDots} from '../../Utils/Svg';
import {Colors} from '../../Utils/Colors';
const touchable = React.createRef();
const CustomeHeader = ({
  title = {},
  title2 = '',
  title3 = '',
  backButton = false,
  companyButton = false,
  backPress = () => {},
  UserDetail = false,
  editIcon = false,
  editPress = () => {},
  listButton = false,
  listButtonPress = () => {},
  menuButton = false,
  menuPress = () => {},
  cancelButton = false,
  cancelPress = () => {},
  TooltipView = {},
}) => {
  return (
    // <MenuProvider>
    <View style={styles.container}>
      <View style={UserDetail ? styles.myStyle1 : styles.myStyle2}>
        {UserDetail ? (
          <>
            <View style={{flexDirection: 'row'}}>
              <Usericon height={90} width={90} />
              {editIcon ? (
                <TouchableOpacity
                  style={{justifyContent: 'flex-end'}}
                  onPress={editPress}>
                  <Edit fill={Colors.orange} />
                </TouchableOpacity>
              ) : (
                <View style={{justifyContent: 'flex-end'}} onPress={editPress}>
                  <Edit fill={Colors.lightblue} />
                </View>
              )}
            </View>
            <Text style={styles.boldtextStyle}>Naser Al Zier</Text>
          </>
        ) : (
          <>
            <Text style={styles.text1Style}>{title}</Text>
            <Text style={styles.text2Style}>{title2}</Text>
          </>
        )}
      </View>

      <View style={styles.container2Style}>
        {backButton ? (
          <Text style={styles.backButtonStyle} onPress={backPress}>
            Back
          </Text>
        ) : cancelButton ? (
          <Text style={styles.backButtonStyle} onPress={cancelPress}>
            Cancel
          </Text>
        ) : (
          <Text style={styles.text3Style}>{title3}</Text>
        )}
        {companyButton ? <Building /> : null}
        {listButton ? (
          <TouchableOpacity onPress={listButtonPress}>
            <List />
          </TouchableOpacity>
        ) : null}
        {/* {menuButton ? (
          <TouchableOpacity onPress={menuPress}>
            {<TooltipView />}
          </TouchableOpacity>
        ) : null} */}
      </View>
    </View>
  );
};
export default CustomeHeader;
