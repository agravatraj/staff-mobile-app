/* eslint-disable react/no-string-refs */
import React from 'react';
import {View, Text, TextInput} from 'react-native';
import styles from './styles';
import {Colors} from '../../Utils/Colors';
import {Dimens} from '../../Utils/Dimens';

class CustomeTextInput extends React.Component {
  static defaultProps = {
    title: '',
    secureTextEntry: false,
    editable: true,
    placeholder: '',
    value: '',
    returnKeyType: 'done',
    blurOnSubmit: true,
    textSize: 'regular', //regular, medium
    fontSize: Dimens.dimen_12,
    fontColor: Colors.white,
    editTextFontSize: Dimens.dimen_12,
    placeholderTextColor: Colors.light,
    numberOfLines: 1,
    multiline: false,
    elevationView: false,
  };
  focus() {
    this.refs.textInput.focus();
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.inputViewStyle}>
          <TextInput
            ref={'textInput'}
            style={styles.TextInputStyle}
            placeholder={this.props.placeholder}
            placeholderTextColor={this.props.placeholderTextColor}
            autoCapitalize="none"
            numberOfLines={this.props.numberOfLines}
            maxLength={this.props.maxLength}
            multiline={this.props.multiline}
            spellCheck={false}
            autoCorrect={false}
            editable={this.props.editable}
            value={this.props.value}
            secureTextEntry={this.props.secureTextEntry}
            keyboardType={this.props.keyboardType}
            returnKeyType={this.props.returnKeyType}
            blurOnSubmit={this.props.blurOnSubmit}
            onSubmitEditing={this.props.onSubmitEditing}
            onChangeText={this.props.onChangeText}
          />
        </View>
        <View style={styles.nameViewStyle}>
          <Text style={styles.nameStyle}>{this.props.title}</Text>
        </View>
      </View>
    );
  }
}
export default CustomeTextInput;
