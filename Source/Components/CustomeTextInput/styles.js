import {StyleSheet} from 'react-native';
import {Colors} from '../../Utils/Colors';
import fonts from '../../Utils/Fonts';
import {Dimens} from '../../Utils/Dimens';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    height: Dimens.dimen_50,
    borderRadius: Dimens.dimen_25,
    alignItems: 'center',
    marginVertical: Dimens.dimen_5,
  },
  TextInputStyle: {
    paddingLeft: Dimens.dimen_160,
    color: Colors.white,
    fontSize: Dimens.dimen_12,
    fontFamily: fonts.openSansBold,
  },
  nameViewStyle: {
    height: Dimens.dimen_50,
    width: Dimens.dimen_160,
    backgroundColor: Colors.light,
    justifyContent: 'center',
    paddingLeft: Dimens.dimen_25,
    borderRadius: Dimens.dimen_25,
    position: 'absolute',
  },
  nameView2Style: {
    height: Dimens.dimen_50,
    width: Dimens.dimen_160,
    backgroundColor: Colors.White,
    justifyContent: 'center',
    paddingLeft: Dimens.dimen_25,
    borderRadius: Dimens.dimen_25,
    position: 'absolute',
    elevation: Dimens.dimen_1,
    shadowColor: Colors.black,
    shadowOffset: {width: 0, height: Dimens.dimen_3},
    shadowOpacity: 0.5,
    shadowRadius: Dimens.dimen_5,
  },
  inputViewStyle: {
    flex: 1,
    height: Dimens.dimen_50,
    justifyContent: 'center',
    paddingHorizontal: Dimens.dimen_20,
    borderRadius: Dimens.dimen_25,
    backgroundColor: Colors.semiWhite,
  },
  nameStyle: {
    color: Colors.lightblue,
    fontSize: Dimens.dimen_14,
    fontFamily: fonts.openSansBold,
  },
});

export default styles;
